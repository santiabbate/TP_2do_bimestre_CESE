/**
 * @file rn2903.h
 * @author Nicolás Bertolo
 * @brief RN2903
 * @version 0.1
 * @date 2019-11-06
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _RN2903_H_
#define _RN2903_H_

#include "uart.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Enum con los diferentes resultados posibles de una operacion de
 * transferencia
 */
typedef enum {
  rn2903_tx_ok = 0,
  rn2903_tx_error = 1,
} rn2903_tx_status_e;

/** Enum con los diferentes resultados posibles de una operacion de
 * recepcion
 */
typedef enum {
  rn2903_rx_ok = 0,
  rn2903_rx_timeout = 1,
  rn2903_rx_error = 2,
} rn2903_rx_status_e;

/** Enum con los diferentes resultados posibles de una operacion de
 * ejecución de comandos
 */
typedef enum {
  rn2903_cmd_ok = 0,
  rn2903_cmd_too_long = 1,
  rn2903_cmd_error = 2,
} rn2903_cmd_status_e;

typedef enum {
  rn2903_init_ok = 0,
  rn2903_init_err = 1,
} rn2903_init_status_e;

/** Definiciones de tipos de funciones para callback */
typedef void ( *rn2903_cmd_callback_fn )( void *, rn2903_cmd_status_e );
typedef void ( *rn2903_tx_callback_fn )( void *, rn2903_tx_status_e );
typedef void ( *rn2903_init_callback_fn )( void *, rn2903_init_status_e );
typedef void ( *rn2903_rx_callback_fn )( void *, rn2903_rx_status_e, const uint8_t *buffer, int16_t len );

// FIXME: Mover a otro header.
typedef void ( *notification_callback_fn )( void * );

#ifdef __cplusplus
}

#ifdef SWIG
%feature("director") rn2903_cmd_cb_wrapper;
%constant void call_rn2903_cmd_cb_wrapper(void * ctx, rn2903_cmd_status_e status);
%feature("director") rn2903_rx_cb_wrapper;
%constant void call_rn2903_rx_cb_wrapper(void *ctx, rn2903_rx_status_e status, const uint8_t *buffer, int len);
%feature("director") rn2903_tx_cb_wrapper;
%constant void call_rn2903_tx_cb_wrapper(void * ctx, rn2903_tx_status_e status);
%feature("director") rn2903_init_cb_wrapper;
%constant void call_rn2903_init_cb_wrapper(void * ctx, rn2903_init_status_e status);
#endif

struct rn2903_cmd_cb_wrapper {
  virtual ~rn2903_cmd_cb_wrapper() {}
  virtual void operator()( rn2903_cmd_status_e ) = 0;
};

struct rn2903_rx_cb_wrapper {
  virtual ~rn2903_rx_cb_wrapper() {}
  virtual void operator()( rn2903_rx_status_e, const uint8_t *, int ) = 0;
};

struct rn2903_tx_cb_wrapper {
  virtual ~rn2903_tx_cb_wrapper() {}
  virtual void operator()( rn2903_tx_status_e ) = 0;
};

struct rn2903_init_cb_wrapper {
  virtual ~rn2903_init_cb_wrapper() {}
  virtual void operator()( rn2903_init_status_e ) = 0;
};

extern "C" {

inline void call_rn2903_cmd_cb_wrapper( void *ctx, rn2903_cmd_status_e status ) {
  auto *cb = (rn2903_cmd_cb_wrapper *)ctx;
  ( *cb )( status );
}

inline void call_rn2903_cmd_callback( rn2903_cmd_callback_fn fn, void *ctx, rn2903_cmd_status_e status ) {
  fn( ctx, status );
}

inline void call_rn2903_rx_cb_wrapper( void *ctx, rn2903_rx_status_e status, const uint8_t *buffer, int len ) {
  auto *cb = (rn2903_rx_cb_wrapper *)ctx;
  ( *cb )( status, buffer, len );
}

inline void call_rn2903_rx_callback( rn2903_rx_callback_fn fn, void *ctx, rn2903_rx_status_e status,
                                     const uint8_t *buffer, int len ) {
  fn( ctx, status, buffer, len );
}

inline void call_rn2903_tx_cb_wrapper( void *ctx, rn2903_tx_status_e status ) {
  auto *cb = (rn2903_tx_cb_wrapper *)ctx;
  ( *cb )( status );
}

inline void call_rn2903_tx_callback( rn2903_tx_callback_fn fn, void *ctx, rn2903_tx_status_e status ) {
  fn( ctx, status );
}

inline void call_rn2903_init_cb_wrapper( void *cinit, rn2903_init_status_e status ) {
  auto *cb = (rn2903_init_cb_wrapper *)cinit;
  ( *cb )( status );
}

inline void call_rn2903_init_callback( rn2903_init_callback_fn fn, void *cinit, rn2903_init_status_e status ) {
  fn( cinit, status );
}

#endif

#ifndef RN2903_MAX_MSG_LENGTH
#define RN2903_MAX_MSG_LENGTH (32+6)
#endif

#define RN2903_BUFFER_LENGTH (9 + 2 * RN2903_MAX_MSG_LENGTH +2)
/** Buffer para la transmisión y recepción */
typedef struct {
  uart_fifo_t uart_buffer;
  int16_t length;
  int16_t send_ix;
  uint8_t buffer[RN2903_BUFFER_LENGTH];
} rn2903_txrx_buffer_t;

typedef enum {
  rn2903_state_cmd_issue,
  rn2903_state_cmd_reply,
  rn2903_state_cmd_callback,
  rn2903_state_idle,
  rn2903_state_rx_cmd,
  rn2903_state_rx_wait_reply,
  rn2903_state_rx_decode_reply,
  rn2903_state_rx_callback,
  rn2903_state_tx_cmd,
  rn2903_state_tx_data,
  rn2903_state_tx_wait_reply,
  rn2903_state_tx_callback,
  rn2903_state_dead,
} rn2903_state_e;

struct rn2903_com_t {
  rn2903_state_e status;
  rn2903_state_e next_status;
  uart_handle_t uart;

  rn2903_tx_callback_fn tx_callback;
  void *tx_callback_ctx;
  rn2903_tx_status_e tx_callback_status;

  rn2903_rx_callback_fn rx_callback;
  void *rx_callback_ctx;
  rn2903_rx_status_e rx_callback_status;
  uint16_t lora_symbols;

  rn2903_cmd_callback_fn cmd_callback;
  void *cmd_callback_ctx;
  rn2903_cmd_status_e cmd_callback_status;

  rn2903_txrx_buffer_t tx_buffer;
  rn2903_txrx_buffer_t rx_buffer;

  notification_callback_fn notification_cb;
  void *notification_ctx;

  rn2903_init_callback_fn init_cb;
  void *init_cb_ctx;
  int init_done;
  int init_cb_called;

  int8_t reset_cmd_ix;
  int8_t expecting_fifo_empty_cb;
};

int rn2903_init_com( struct rn2903_com_t *com, uart_handle_t uart, notification_callback_fn notification_cb,
                     void *notification_ctx, rn2903_init_callback_fn init_cb, void *init_cb_ctx );

#ifdef SWIG
%pybuffer_binary(const uint8_t *data, int16_t size)
#endif
/** Inicia una operación de transferencia. Cuando se complete u ocurra un *
 * error se llama a `callback` con `context` como primer argumento.
 */
int rn2903_send( struct rn2903_com_t *com, rn2903_tx_callback_fn callback, void *context, const uint8_t *data,
                 int16_t size );

/** Inicia una operación de recepción. Cuando se complete u ocurra un error
 * se llama a `callback` con `context` como primer argumento.
 */
int rn2903_receive( struct rn2903_com_t *com, uint16_t lora_symbols, rn2903_rx_callback_fn callback, void *context );

#ifdef SWIG
%pybuffer_binary(const char *data, int16_t size)
#endif
/** Inicia una operación de comando. Cuando se complete u ocurra un error
 * se llama a `callback` con `context` como primer argumento.
 * El comando debe terminar en CRLF.
 */
int rn2903_issue_cmd( struct rn2903_com_t *com, rn2903_cmd_callback_fn callback, void *context, const char *data,
                      int16_t size );

/** Llama a los callbacks si es necesario */
int rn2903_run_one_iteration( struct rn2903_com_t *com );

/** Retorna si el dispositivo fue inicializado correctamente */
int rn2903_init_done( struct rn2903_com_t *com );

/** Retorna si el dispositivo está en un estado de error irrecuperable. */
int rn2903_is_dead( struct rn2903_com_t *com );

#ifdef __cplusplus
}
#endif

#endif
