#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void debug_print( void *prefix, void *msg );

void debug_num( void *prefix, long num );

void debug_str( void *prefix, const char *str, int len );

#ifdef __cplusplus
}
#endif
