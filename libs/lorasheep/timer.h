/**
 * @file timer.h
 * @author Nicolás Bertolo
 * @author Santiago Abbate (Documentación)
 * @brief 
 * @version 0.1
 * @date 2019-11-06
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _TIMER_H_
#define _TIMER_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void *timer_handle_t;

/** Definiciones de tipos de funciones de callback */
typedef void ( *timer_synch_start_callback_fn )( void * );

/** @name Group2
 *  Punteros a funciones que una implementación del timer tiene que implementar mandatoriamente
 */
/**@{*/ 
/** Función synch_start. Inicia un timer con un `period` respecto al segundo 0.000 (UTC) del día.
      Agrega una desfasaje adicional a través del la opción `delay_ms`. Cuando hay `timeout`del 
      timer se ejecuta el `callback`*/
typedef int ( *timer_synch_start_impl_fn )( void *timer_private, int16_t period, int16_t delay_ms,
                                           timer_synch_start_callback_fn callback, void *ctx );
/**@}*/


/** Cancela el timer. Si hay una ejecución pendiente también se cancela.*/
/**@{*/
typedef int ( *timer_cancel_impl_fn )( void *timer_private );
/**@}*/

/** Actualiza el estado del timer. Si hubo `timeout` llama al callback. */
/**@{*/
typedef int ( *timer_run_one_iteration_impl_fn )( void *timer_private );
/**@}*/

/**
 * @struct timer_iface_t
 * @var _private Miembro privado de la estructura. Para ser usado por la implementación del timer según sea necesario
 * @var _synch_start_fn Función de sincronización de la implementación del timer
 * @var _cancel_fn Cancela el timer.
 * @var _run_one_iteration_fn ACtualiza el estado del timer. Llama al callback si hubo timeout
 */
typedef struct {
  void *_private;
  timer_synch_start_impl_fn _synch_start_fn;
  timer_cancel_impl_fn _cancel_fn;
  timer_run_one_iteration_impl_fn _run_one_iteration_fn;
} timer_iface_t;


// Directivas a SWIG de pyhton
#ifdef __cplusplus
}
#ifdef SWIG
%feature("director") timer_synch_start_cb_wrapper;
%constant void call_timer_synch_start_cb_wrapper( void *ctx );
#endif
struct timer_synch_start_cb_wrapper {
  virtual ~timer_synch_start_cb_wrapper() {}
  virtual void operator()() = 0;
};

extern "C" {

inline void call_timer_synch_start_cb_wrapper( void *ctx ) {
  auto *cb = (timer_synch_start_cb_wrapper *)ctx;
  ( *cb )();
}
#endif

inline void call_timer_synch_start_callback( timer_synch_start_callback_fn fn, void *ctx ) { fn( ctx ); }

/** Ejecuta el callback de synch_start del timer  */
inline int timer_synch_start( timer_handle_t handle, int16_t period, int16_t delay_ms,
                              timer_synch_start_callback_fn callback, void *ctx ) {
  timer_iface_t *timer = (timer_iface_t *)handle;
  return timer->_synch_start_fn( timer->_private, period, delay_ms, callback, ctx );
}

/** Cancela el timer. Si hay una ejecución pendiente también se cancela. */
inline int timer_cancel( timer_handle_t handle ) {
  timer_iface_t *timer = (timer_iface_t *)handle;
  return timer->_cancel_fn( timer->_private );
}

inline int timer_run_one_iteration( timer_handle_t handle ) {
  timer_iface_t *timer = (timer_iface_t *)handle;
  return timer->_run_one_iteration_fn( timer->_private );
}

#ifdef __cplusplus
}
#endif

#endif
