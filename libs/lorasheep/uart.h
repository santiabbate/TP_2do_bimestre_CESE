/**
 * @file uart.h
 * @author Nicolás Bertolo
 * @brief 
 * @version 0.1
 * @date 2019-11-06
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _UART_H_
#define _UART_H_

#include "uart_fifo.h"
#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void *uart_handle_t;

/**
 * Definiciones de estados que puede devolver la recepción por la UART. Los valores aumentan
 * según su severidad
 */
enum uart_rx_status_e {
  uart_rx_ok = 0,
  uart_rx_timeout = 1,
  uart_rx_buffer_overrun = 2,
  uart_rx_error = 3,
};

/**
 * Definiciones de estados que puede devolver el callback de transmisión. Los valores aumentan
 * según su severidad
 */
enum uart_tx_status_e {
  uart_tx_fifo_empty = 0,
  uart_tx_error = 1,
};

/** Definiciones de tipos de callback */
typedef void ( *uart_rx_callback_fn )( void *, enum uart_rx_status_e );
typedef void ( *uart_tx_callback_fn )( void *, enum uart_tx_status_e );

#ifdef __cplusplus
}

#ifdef SWIG
%feature("director") uart_rx_callback;
%constant void call_uart_rx_cb_wrapper(void * ctx, uart_rx_status_e status);
%feature("director") uart_tx_cb_wrapper;
%constant void call_uart_tx_cb_wrapper(void * ctx, uart_tx_status_e status);
#endif
struct uart_rx_cb_wrapper {
  virtual ~uart_rx_cb_wrapper() {}
  virtual void operator()( uart_rx_status_e ) = 0;
};

struct uart_tx_cb_wrapper {
  virtual ~uart_tx_cb_wrapper() {}
  virtual void operator()( uart_tx_status_e ) = 0;
};

extern "C" {

inline void call_uart_rx_cb_wrapper( void *ctx, uart_rx_status_e status ) {
  auto *cb = (uart_rx_cb_wrapper *)ctx;
  ( *cb )( status );
}

inline void call_uart_rx_callback( uart_rx_callback_fn fn, void *ctx, uart_rx_status_e status ) { fn( ctx, status ); }

inline void call_uart_tx_cb_wrapper( void *ctx, uart_tx_status_e status ) {
  auto *cb = (uart_tx_cb_wrapper *)ctx;
  ( *cb )( status );
}

inline void call_uart_tx_callback( uart_tx_callback_fn fn, void *ctx, uart_tx_status_e status ) { fn( ctx, status ); }
#endif

typedef int ( *uart_begin_write_impl_fn )( void *uart_private, uart_fifo_t *fifo, uart_tx_callback_fn callback,
                                           void *ctx );

typedef int ( *uart_begin_readline_impl_fn )( void *uart_private, uart_fifo_t *buff, uint8_t readlines,
                                              uart_rx_callback_fn callback, void *ctx, int32_t timeout_ms );

typedef int ( *uart_close_fn )( void *uart_private );

typedef int ( *uart_run_one_iteration_impl_fn )( void *uart_private );

/**
 * @struct  Interfaz de la UART que es visible al protocolo
 * _private Miembro privado de la estructura. Para ser usado por la implementación de la UART según sea necesario
 * _close Cierra la UART
 * _begin_readline Inicia la lectura de un número de líneas por la UART. Espera un "\r\n"
 * _begin write Inicia una escritura
 */
typedef struct {
  void *_private;
  uart_run_one_iteration_impl_fn _run_one_iteration;
  uart_close_fn _close;
  uart_begin_readline_impl_fn _begin_readline;
  uart_begin_write_impl_fn _begin_write;
} uart_iface_t;

/**
 * Funciones públicas
 */

/** Inicia una lectura de linea.
 *
 *  Cuando se complete u ocurra un error se llama a `callback` con `context`
 * como primer argumento. Se puede definir un timeout en ms y la cantidad de
 * lineas que se desea leer.
 */

/**
 * @brief Inicia una lectura de línea (o líneas) a través de la interfaz UART. Cuando termina la lectura llama a `callback` con
 * `context` como primer argumento. Si hubo timeout también llama al callback informando el timeout.
 * 
 * @param handle Handle a la interfaz UART en uso
 * @param buff  Bueffer de recepción 
 * @param readlines Cantidad de líneas a leer
 * @param callback Callback al finalizar la recepción
 * @param ctx Callback context
 * @param timeout_ms Tiempo de timeout en la espera de la línea
 * @return int 
 * @ingroup uartFn
 */
inline int uart_begin_readline( uart_handle_t handle, uart_fifo_t *buff, uint8_t readlines,
                                uart_rx_callback_fn callback, void *ctx, int32_t timeout_ms ) {
  uart_iface_t *uart = (uart_iface_t *)handle;
  return uart->_begin_readline( uart->_private, buff, readlines, callback, ctx, timeout_ms );
}

/** Inicia una escritura. Cuando se vacie la FIFO se llama a `callback` con
 * `context` como primer argumento.
 */
inline int uart_begin_write( uart_handle_t handle, uart_fifo_t *fifo, uart_tx_callback_fn callback, void *ctx ) {
  uart_iface_t *uart = (uart_iface_t *)handle;
  return uart->_begin_write( uart->_private, fifo, callback, ctx );
}

/** Termina la comunicación */
inline int uart_close( uart_handle_t handle ) {
  uart_iface_t *uart = (uart_iface_t *)handle;
  return uart->_close( uart->_private );
}

/** Actualiza el estado de la UART. Llama a los callbacks si es necesario */
inline int uart_run_one_iteration( uart_handle_t handle ) {
  uart_iface_t *uart = (uart_iface_t *)handle;
  return uart->_run_one_iteration( uart->_private );
}

#ifdef __cplusplus
}
#endif

#endif
