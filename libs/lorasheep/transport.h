#ifndef _TRANSPORT_H
#define _TRANSPORT_H
/**
 * @file transport.h
 * @author Nicolás Bertolo
 * @brief 
 * @version 0.1
 * @date 2019-11-06
 * 
 * @copyright Copyright (c) 2019
 * @page transport Capa de Transporte
 * 
 */

/** @mainpage My Personal Index Page
 *
 * @section intro_sec Introduction
 *
 * This is the introduction.
 *
 * @section install_sec Installation
 *
 * @subsection step1 Step 1: Opening the box
 *  
 * etc...
 */


#include "datalink.h"
#include "timer.h"

#ifdef __cplusplus
extern "C" {
#endif

enum transport_state_e {
  transport_state_init,
  transport_state_idle,
  transport_state_dead,

  transport_state_tx,
  transport_state_tx_frame_sent,
  transport_state_tx_callback,

  transport_state_rx,
  transport_state_rx_wait,
  transport_state_rx_callback,
};

/** Enum con los resultados posibles de inicialización  */
enum transport_init_result_e { transport_init_ok = 0, transport_init_err = 1 };

/** Enum con los resultados posibles de transmisión  */
enum transport_tx_result_e { transport_tx_ok = 0, transport_tx_no_ack = 1, transport_tx_err = 2 };

/** Enum con los resultados posibles de recepción  */
enum transport_rx_result_e { transport_rx_ok = 0, transport_rx_err = 1 };

/** Definiciones de tipos de funciones para callback */
typedef void ( *transport_init_callback_fn )( void *, enum transport_init_result_e );
typedef void ( *transport_tx_callback_fn )( void *, enum transport_tx_result_e );
typedef void ( *transport_rx_callback_fn )( void *, enum transport_rx_result_e, int16_t size, lora_addr_t src );

#ifdef __cplusplus
}

#ifdef SWIG
%feature("director") transport_init_cb_wrapper;
%constant void call_transport_init_cb_wrapper(void * ctx, transport_init_result_e status);
%feature("director") transport_tx_cb_wrapper;
%constant void call_transport_tx_cb_wrapper(void * ctx, transport_tx_result_e status);
%feature("director") transport_rx_cb_wrapper;
%constant void call_transport_rx_cb_wrapper(void * ctx, transport_rx_result_e status, int16_t size, lora_addr_t src);
#endif

struct transport_init_cb_wrapper {
  virtual ~transport_init_cb_wrapper() {}
  virtual void operator()( transport_init_result_e ) = 0;
};

struct transport_tx_cb_wrapper {
  virtual ~transport_tx_cb_wrapper() {}
  virtual void operator()( transport_tx_result_e ) = 0;
};

struct transport_rx_cb_wrapper {
  virtual ~transport_rx_cb_wrapper() {}
  virtual void operator()( transport_rx_result_e, int16_t, lora_addr_t src ) = 0;
};

extern "C" {

inline void call_transport_init_callback( transport_init_callback_fn fn, void *ctx, transport_init_result_e status ) {
  fn( ctx, status );
}

inline void call_transport_init_cb_wrapper( void *ctx, transport_init_result_e status ) {
  auto *cb = (transport_init_cb_wrapper *)ctx;
  ( *cb )( status );
}

inline void call_transport_tx_callback( transport_tx_callback_fn fn, void *ctx, transport_tx_result_e status ) {
  fn( ctx, status );
}

inline void call_transport_tx_cb_wrapper( void *ctx, transport_tx_result_e status ) {
  auto *cb = (transport_tx_cb_wrapper *)ctx;
  ( *cb )( status );
}

inline void call_transport_rx_callback( transport_rx_callback_fn fn, void *ctx, transport_rx_result_e status,
                                        int16_t size, lora_addr_t src ) {
  fn( ctx, status, size, src );
}

inline void call_transport_rx_cb_wrapper( void *ctx, transport_rx_result_e status, int16_t size, lora_addr_t src ) {
  auto *cb = (transport_rx_cb_wrapper *)ctx;
  ( *cb )( status, size, src );
}
#endif

/**
 * @brief Estructura de control de la capa de transporte
 * 
 */
struct transport_t {
  enum transport_state_e state;
  enum transport_state_e next_state;
  struct lora_datalink_t dl;

  notification_callback_fn notification_cb;
  void *notification_ctx;

  transport_init_callback_fn init_cb;
  void *init_cb_ctx;
  uint8_t init_done;

  uint8_t *rx_buffer;
  const uint8_t *tx_buffer;
  int8_t seq_number;
  int16_t index;
  int16_t remaining_data_length;

  enum transport_tx_result_e tx_result;
  transport_tx_callback_fn tx_cb;
  void *tx_cb_ctx;
  lora_addr_t dst;
  int8_t tx_enabled;

  enum transport_rx_result_e rx_result;
  transport_rx_callback_fn rx_cb;
  lora_addr_t rx_src;
  void *rx_cb_ctx;
  int8_t rx_enabled;
  uint8_t rx_retries;
};

/**
 * @brief Inicializa la capa de transporte, recibe las implementaciones correspondientes de la plataforma y los callbacks que fueran necesarios
 * 
 * @param tp Puntero a estructura de control de la capa
 * @param uart Puntero a implementación de la UART de la plataforma
 * @param timer Puntero a implementación del Timer de la plataforma
 * @param notification_cb Función de callback de notificación de la capa
 * @param notification_ctx Parámetros que recibirá el callback de notificación de la capa
 * @param cb Función de callback una vez que se inicialió la capa
 * @param init_cb_ctx Parámetros que recibirá el callback de inicialización de la capa
 * @return int 
 */
int transport_init( struct transport_t *tp, uart_handle_t uart, timer_handle_t timer,
                    notification_callback_fn notification_cb, void *notification_ctx, transport_init_callback_fn cb,
                    void *init_cb_ctx );

/**
 * @brief Envía una serie de bytes. Llama al callback pasado por parámetros una vez finaliza la transmisión
 * 
 * @param tp Puntero a estructura de control de la capa
 * @param dst Dirección numérica del módulo destino de los datos
 * @param data Buffer de datos a transmitir
 * @param size Tamaño del buffer, en cantidad de bytes
 * @param cb Función de callback a ejecutar luego de la transmisión
 * @param tx_cb_ctx Parámetros que recibirá el callback de transmisión
 * @return int 
 */
int transport_send( struct transport_t *tp, lora_addr_t dst, const uint8_t *data, int16_t size,
                    transport_tx_callback_fn cb, void *tx_cb_ctx );

/**
 * @brief Retorna la longitud máxima de un paquete
 * 
 * @param tp Puntero a estructura de control de la capa
 * @return int Longitud máxima permitida para el paquete a enviar
 */
int transport_max_packet_length( struct transport_t *tp );

#ifdef SWIG
%pybuffer_mutable_binary( uint8_t*dstdata, int16_t size)
#endif

/**
 * @brief Recibe una serie de bytes. Llama al callback pasado por parámetros una vez finaliza la recepción
 * 
 * @param tp Puntero a estructura de control de la capa
 * @param dstdata Buffer de recepción de los datos
 * @param size Tamaño del buffer, en cantidad de bytes
 * @param src Dirección numérica del módulo del que se quiere recibir datos
 * @param cb Función de callback a ejecutar luego de la recepción
 * @param rx_cb_ctx Parámetros que recibirá el callback de recepción
 * @return int 
 */
int transport_receive( struct transport_t *tp, uint8_t *dstdata, int16_t size, lora_addr_t src,
                       transport_rx_callback_fn cb, void *rx_cb_ctx );

/**
 * @brief Actualiza el estado de la capa de transporte. Llama a los callbacks necesarios si estuvieran inicializados
 * 
 * @param tp Puntero a estructura de control de la capa
 * @return int
 */
int transport_run_one_iteration( struct transport_t *tp );

/**
 * @brief Retorna si la capa de transporte fue inicializada correctamente
 * 
 * @param tp Puntero a estructura de control de la capa
 * @return int 
 * @retval 0 Transporte inicializada correctamente
 * @retval 1 Transpote no inicializada
 */
int transport_init_done( struct transport_t *tp );

/**
 * @brief Retorna si el transporte está en un estado de error irrecuperable
 * 
 * @param tp Puntero a estructura de control de la capa
 * @return int 
 * @retval 0 Transporte activa
 * @retval 1 Transpote caída
 */
int transport_is_dead( struct transport_t *tp );

#ifdef __cplusplus
}
#endif
#endif
