#include "debug_print.h"
#include "transport.h"
#include <string.h>
#ifdef TRANSPORT_DEBUG
void _transport_error( const char *errmsg ) { printf( "TRANSPORT ERROR: %s\n", errmsg ); }
void _transport_debug( const char *errmsg ) { printf( "TRANSPORT DEBUG: %s\n", errmsg ); }
#elif ARDUINO
#include <avr/pgmspace.h>
#define _transport_error( msg ) debug_print( PSTR( "TRANSPORT ERROR" ), PSTR(msg) )
#define _transport_debug( msg ) debug_print( PSTR( "TRANSPORT DEBUG" ), PSTR(msg) )
/* #define _transport_error( ... ) */
/* #define _transport_debug( ... ) */
#else
void _transport_error( const char *errmsg ) {}
void _transport_debug( const char *errmsg ) {}
#endif

#define NEXT_STATE( state )                                                                                            \
  tp->next_state = state;                                                                                              \
  _transport_debug( "nuevo estado " #state );                                                                          \
  if ( !tp->notification_cb ) {                                                                                        \
    _transport_error( "no hay callback de notificacion" );                                                             \
  } else {                                                                                                             \
    tp->notification_cb( tp->notification_ctx );                                                                       \
  }

#define DIE( msg )                                                                                                     \
  _transport_error( msg );                                                                                             \
  NEXT_STATE( transport_state_dead );

void _transport_datalink_receive_cb( void *ctx, enum lora_datalink_rx_result_e status, const uint8_t *buffer,
                                     int16_t length, uint8_t seq_number, lora_addr_t src ) {
  struct transport_t *tp = ctx;

  switch ( status ) {
  case lora_datalink_rx_ok: {
    _transport_debug( "rx_ok" );
    if ( tp->rx_enabled ) {
      memcpy( tp->rx_buffer + tp->index, buffer, length );
      tp->index += length;
      if ( tp->seq_number == -1 ) {
        _transport_debug( "iniciamos recepción de un paquete nuevo" );
        tp->rx_src = src;
        tp->seq_number = seq_number;
      } else {
        _transport_debug( "recibimos frame parte de un paquete" );
      }
      if ( tp->seq_number == 0 ) {
        _transport_debug( "Recibimos paquete completo" );
        NEXT_STATE( transport_state_rx_callback );
      } else {
        if ( tp->seq_number > 0 ) {
          tp->seq_number--;
        }
        NEXT_STATE( transport_state_rx );
      }
    } else {
      _transport_debug( "descartando frame" );} break;
  }

  case lora_datalink_rx_timeout: {
    _transport_debug( "rx_timeout" );
    if ( tp->rx_enabled ) {
      if ( tp->rx_retries < 10 && tp->seq_number >= 0 ) {
        tp->rx_retries++;
        _transport_debug( "timeout en recepción. reprobando" );
      } else {
        if ( tp->seq_number >= 0 ) {
          _transport_debug( "abandonando recepción de paquete. buscando primer frame de secuencia" );
        }
        tp->rx_retries = 0;
        tp->seq_number = -1;
        tp->index = 0;
        tp->remaining_data_length = 0;
      }
      NEXT_STATE( transport_state_rx );
    } else {
      _transport_debug( "rx disabled" );
    }
    break;
  }

  case lora_datalink_rx_err: {
    DIE( "error en la recepción de un frame" );
    break;
  }
  }
}

void _transport_datalink_init_cb( void *ctx, enum lora_datalink_init_result_e status ) {
  struct transport_t *tp = ctx;
  if ( tp->state == transport_state_init ) {
    switch ( status ) {
    case lora_datalink_init_ok: {
      _transport_debug( "inicialización completa" );
      NEXT_STATE( transport_state_idle );
      break;
    }
    case lora_datalink_init_err: {
      DIE( "fallo en inicialización de datalink" );
      break;
    }
    }
  } else {
    DIE( "callback de datalink init cuando no estamos en estado adecuado" );
  }
}

void _transport_datalink_transmit_cb( void *ctx, enum lora_datalink_tx_result_e status ) {
  struct transport_t *tp = ctx;
  if ( tp->state == transport_state_tx ) {
    switch ( status ) {
    case lora_datalink_tx_ok: {
      _transport_debug( "frame transmitido exitosamente" );
      NEXT_STATE( transport_state_tx_frame_sent );
      break;
    }

    case lora_datalink_tx_err: {
      DIE( "error de transmisión de datalink" );
      break;
    }

    case lora_datalink_tx_no_ack:
      _transport_debug( "paquete no tuvo ACK" );
      tp->tx_result = transport_tx_no_ack;
      NEXT_STATE( transport_state_tx_callback );
      break;
    }
  } else {
    DIE( "callback de datalink transmitir cuando no estamos en transport_state_tx" );
  }
}

void _transport_handle_new_state( struct transport_t *tp ) {
  if ( tp->next_state == tp->state ) {
    return;
  }
  tp->state = tp->next_state;

  switch ( tp->state ) {
  case transport_state_init: {
    break;
  }

  case transport_state_idle: {
    if ( !tp->init_done ) {
      _transport_debug( "transport init done" );
      tp->init_done = 1;
      tp->seq_number = -1;
      /* int ret = lora_datalink_receive( &tp->dl, -1, 1, _transport_datalink_receive_cb, tp ); */
      if ( tp->init_cb ) {
        tp->init_cb( tp->init_cb_ctx, transport_init_ok );
      }
    }
    break;
  }

  case transport_state_dead: {
    if ( !tp->init_done ) {
      tp->init_done = 1;
      if ( tp->init_cb ) {
        tp->init_cb( tp->init_cb_ctx, transport_init_err );
      }
    } else {
      transport_tx_callback_fn tx_cb = tp->tx_cb;
      tp->tx_cb = 0;
      if ( tx_cb ) {
        tx_cb( tp->tx_cb_ctx, transport_tx_err );
      } else {
        _transport_debug( "no hay callback de tx" );
      }
      transport_rx_callback_fn rx_cb = tp->rx_cb;
      tp->rx_cb = 0;
      if ( rx_cb ) {
        rx_cb( tp->rx_cb_ctx, transport_rx_err, 0, 0 );
      } else {
        _transport_debug( "no hay callback de rx" );
      }
    }
    break;
  }

  case transport_state_rx: {
    lora_addr_t src = tp->rx_src;
    int ret = lora_datalink_receive( &tp->dl, tp->seq_number, src, _transport_datalink_receive_cb, tp );
    if ( ret ) {
      DIE( "comienzo de recepción fallido" );
    }
    NEXT_STATE( transport_state_rx_wait );
    break;
  }

  case transport_state_rx_wait: {
    break;
  }

  case transport_state_rx_callback: {
    NEXT_STATE( transport_state_idle );
    transport_rx_callback_fn cb = tp->rx_cb;
    /* tp->rx_cb = 0; */
    if ( cb ) {
      cb( tp->rx_cb_ctx, tp->rx_result, tp->index, tp->rx_src );
    } else {
      _transport_debug( "no hay callback de rx" );
    }
    break;
  }

  case transport_state_tx: {
    int16_t tx_size = tp->remaining_data_length;
    uint8_t is_first_seq = tp->index == 0;
    int16_t max_length = lora_datalink_max_frame_length( &tp->dl );
    tx_size = tx_size > max_length ? max_length : tx_size;
    tp->remaining_data_length -= tx_size;
    int ret = lora_datalink_transmit( &tp->dl, tp->dst, tp->seq_number, is_first_seq, tp->tx_buffer + tp->index,
                                      tx_size, _transport_datalink_transmit_cb, tp );
    tp->index += tx_size;
    tp->seq_number -= 1;
    if ( ret ) {
      DIE( "inicio de transmisión falló" );
    }
    break;
  }

  case transport_state_tx_frame_sent: {
    if ( tp->remaining_data_length ) {
      _transport_debug( "transmitiendo otro frame" );
      NEXT_STATE( transport_state_tx );
    } else {
      _transport_debug( "paquete enviado con éxito" );
      tp->tx_result = transport_tx_ok;
      NEXT_STATE( transport_state_tx_callback );
    }
    break;
  }

  case transport_state_tx_callback: {
    NEXT_STATE( transport_state_idle );
    transport_tx_callback_fn cb = tp->tx_cb;
    tp->tx_cb = 0;
    if ( cb ) {
      cb( tp->tx_cb_ctx, tp->tx_result );
    } else {
      _transport_debug( "no hay callback de tx" );
    }
    break;
  }
  }
}

/** Funciones públicas */

/** Inicializa la capa de transporte */
int transport_init( struct transport_t *tp, uart_handle_t uart, timer_handle_t timer,
                    notification_callback_fn notification_cb, void *notification_ctx, transport_init_callback_fn cb,
                    void *init_cb_ctx ) {
  memset( tp, 0, sizeof( struct transport_t ) );
  tp->state = transport_state_init;
  tp->next_state = transport_state_init;
  tp->notification_cb = notification_cb;
  tp->notification_ctx = notification_ctx;
  tp->init_cb = cb;
  tp->init_cb_ctx = init_cb_ctx;
  return lora_datalink_init( &tp->dl, uart, timer, notification_cb, notification_ctx, _transport_datalink_init_cb, tp );
}

int transport_send( struct transport_t *tp, lora_addr_t dst, const uint8_t *data, int16_t size,
                    transport_tx_callback_fn cb, void *tx_cb_ctx ) {
  if ( !transport_init_done( tp ) ) {
    _transport_error( "inicialización incompleta" );
    return -1;
  }

  if ( tp->state == transport_state_dead ) {
    _transport_error( "estamos dead, no podemos enviar" );
    return -2;
  }

  int16_t max_length_frame = lora_datalink_max_frame_length( &tp->dl );
  int16_t max_length_packet = max_length_frame * ( 1 << DATALINK_SEQ_NUMBER_WIDTH );
  if ( size > max_length_packet ) {
    _transport_error( "datos demasiado grandes" );
    return -3;
  }

  tp->tx_cb = cb;
  tp->tx_cb_ctx = tx_cb_ctx;

  tp->tx_buffer = data;
  tp->remaining_data_length = size;
  tp->index = 0;
  tp->rx_enabled = 0;

  if ( size > 0 ) {
    tp->seq_number = -1 + ( size + max_length_frame - 1 ) / max_length_frame;
  } else {
    tp->seq_number = 0;
  }
  tp->dst = dst;

  NEXT_STATE( transport_state_tx );
  return 0;
}

int transport_max_packet_length( struct transport_t *tp ) {
  int16_t max_length_frame = lora_datalink_max_frame_length( &tp->dl );
  int16_t max_length_packet = max_length_frame * ( 1 << DATALINK_SEQ_NUMBER_WIDTH );
  return max_length_packet;
}

/** Recibe una serie de bytes. */
int transport_receive( struct transport_t *tp, uint8_t *dstdata, int16_t size, lora_addr_t src,
                       transport_rx_callback_fn cb, void *rx_cb_ctx ) {
  if ( !transport_init_done( tp ) ) {
    _transport_error( "inicialización incompleta" );
    return -1;
  }

  if ( transport_is_dead( tp ) ) {
    _transport_error( "estamos dead, no podemos recibir" );
    return -2;
  }

  int16_t max_length_frame = lora_datalink_max_frame_length( &tp->dl );
  int16_t max_length_packet = max_length_frame * ( 1 << DATALINK_SEQ_NUMBER_WIDTH );
  if ( size < max_length_packet ) {
    _transport_error( "buffer demasiado pequeño" );
    return -3;
  }

  tp->rx_buffer = dstdata;
  tp->seq_number = -1;
  tp->rx_cb = cb;
  tp->rx_cb_ctx = rx_cb_ctx;
  tp->rx_enabled = 1;
  tp->rx_src = src;
  tp->index = 0;

  NEXT_STATE( transport_state_rx );
  return 0;
}

/** Llama a callbacks si es necesario. */
int transport_run_one_iteration( struct transport_t *tp ) {
  _transport_handle_new_state( tp );
  return 1;
}

/** Retorna si el transporte fue inicializado correctamente */
int transport_init_done( struct transport_t *tp ) { return tp->init_done; }

/** Retorna si el transporte está en un estado de error irrecuperable. */
int transport_is_dead( struct transport_t *tp ) { return tp->state == transport_state_dead; }
