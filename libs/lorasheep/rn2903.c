#include "rn2903.h"
#include "debug_print.h"
#include "uart.h"
#include <stdio.h>
#include <string.h>

/** Funciones privadas */

#ifdef RN2903_DEBUG
void _rn2903_error( const char *errmsg ) { printf( "RN2903 ERROR: %s\n", errmsg ); }
void _rn2903_debug( const char *errmsg ) { printf( "RN2903 DEBUG: %s\n", errmsg ); }
#elif ARDUINO
#include <avr/pgmspace.h>
#define _rn2903_error( msg ) debug_print( PSTR( "RN2903 ERROR" ), PSTR( msg ) )
#define _rn2903_debug( msg ) debug_print( PSTR( "RN2903 DEBUG" ), PSTR( msg ) )
#else
void _rn2903_error( const char *errmsg ) {}
void _rn2903_debug( const char *errmsg ) {}
#endif

void _rn2903_byte_to_hex( uint8_t byte, uint8_t hex[2] ) {
  const uint8_t nibble_to_hex[16] = "0123456789ABCDEF";
  hex[0] = nibble_to_hex[byte >> 4];
  hex[1] = nibble_to_hex[byte & 0xF];
}

uint8_t _rn2903_digit_to_nibble( uint8_t digit ) {
  switch ( digit ) {
  default: {
    _rn2903_error( "digito hexa desconocido" );
    return 0;
  }
  case '0':
    return 0;
  case '1':
    return 1;
  case '2':
    return 2;
  case '3':
    return 3;
  case '4':
    return 4;
  case '5':
    return 5;
  case '6':
    return 6;
  case '7':
    return 7;
  case '8':
    return 8;
  case '9':
    return 9;
  case 'A':
    return 10;
  case 'B':
    return 11;
  case 'C':
    return 12;
  case 'D':
    return 13;
  case 'E':
    return 14;
  case 'F':
    return 15;
  }
}

uint8_t _rn2903_digits_to_byte( uint8_t leftdigit, uint8_t rightdigit ) {
  uint8_t result;
  result = _rn2903_digit_to_nibble( leftdigit ) << 4;
  result |= _rn2903_digit_to_nibble( rightdigit ) & 0xF;
  return result;
}

void _rn2903_txrx_buffer_init( rn2903_txrx_buffer_t *buff ) {
  uart_fifo_init( &buff->uart_buffer );
  buff->length = 0;
  buff->send_ix = 0;
}

uint8_t _rn2903_rx_window_size_str( uint16_t value, char str[5] ) {
  uint16_t divisor = 10000;
  uint8_t leftdigit;
  uint8_t digits = 0;
  _Bool printed_first_digit = 0;

  for ( int i = 0; i < 5; ++i ) {
    leftdigit = value / divisor;
    value = value % divisor;
    divisor = divisor / 10;
    if ( leftdigit > 0 || i == 4 || printed_first_digit ) {
      str[digits] = '0' + leftdigit;
      digits++;
      printed_first_digit = 1;
    }
  }
  return digits;
}

int _rn2903_has_CRLF_ending( const rn2903_txrx_buffer_t *buffer ) {
  int16_t length = buffer->length;
  if ( length < 2 ) {
    return 0;
  }
  uint8_t cr = buffer->buffer[length - 2];
  uint8_t lf = buffer->buffer[length - 1];
  if ( cr == '\r' && lf == '\n' ) {
    return 1;
  }
  return 0;
}

void _rn2903_debug_print_crlf( const rn2903_txrx_buffer_t *buffer ) {
  /* #ifdef RN2903_DEBUG */
  if ( !_rn2903_has_CRLF_ending( buffer ) ) {
    _rn2903_error( "buffer does not have CRLF ending" );
    return;
  }
  //_rn2903_debug(buffer->buffer);
  //debug_str(PSTR("RN2903 DEBUG BUFFER"), (char*)buffer->buffer, buffer->length);
  /* #endif */
}

int _rn2903_str_prefix_equal( const uint8_t *msg, int16_t length, const uint8_t *prefix, int16_t prefix_length ) {
  if ( length < prefix_length ) {
    return 0;
  }

  int16_t i = 0;
  while ( *prefix != '\0' && i < length ) {
    if ( *prefix != *msg ) {
      return 0;
    }
    prefix++;
    msg++;
    i++;
  }
  if ( *prefix == '\0' ) {
    return 1;
  } else {
    return 0;
  }
}

const uint8_t radio_rx_prefix[] = "radio_rx  ";

int _rn2903_is_ok_reply( const rn2903_txrx_buffer_t *buff ) {
  const uint8_t prefix[] = "ok\r\n";
  return _rn2903_str_prefix_equal( buff->buffer, buff->length, prefix, sizeof( prefix ) - 1 );
}

int _rn2903_is_busy_reply( const rn2903_txrx_buffer_t *buff ) {
  const uint8_t prefix[] = "busy\r\n";
  return _rn2903_str_prefix_equal( buff->buffer, buff->length, prefix, sizeof( prefix ) - 1 );
}

int _rn2903_is_invalid_param_reply( const rn2903_txrx_buffer_t *buff ) {
  const uint8_t prefix[] = "invalid_param\r\n";
  return _rn2903_str_prefix_equal( buff->buffer, buff->length, prefix, sizeof( prefix ) - 1 );
}

int _rn2903_is_reply_tx_ok_reply( const rn2903_txrx_buffer_t *buff ) {
  const uint8_t prefix[] = "radio_tx_ok\r\n";
  return _rn2903_str_prefix_equal( buff->buffer, buff->length, prefix, sizeof( prefix ) - 1 );
}

int _rn2903_is_reply_rx_data_reply( const rn2903_txrx_buffer_t *buff ) {
  return _rn2903_str_prefix_equal( buff->buffer, buff->length, radio_rx_prefix, sizeof( radio_rx_prefix ) - 1 );
}

int _rn2903_is_radio_err_reply( const rn2903_txrx_buffer_t *buff ) {
  const uint8_t prefix[] = "radio_err\r\n";
  return _rn2903_str_prefix_equal( buff->buffer, buff->length, prefix, sizeof( prefix ) - 1 );
}

int _rn2903_xfer_from_uart_to_buffer( rn2903_txrx_buffer_t *rx_buffer ) {
  // Copiamos desde la fifo al buffer de rx.
  uint8_t byte;
  int buffer_overrun = 0;
  while ( uart_fifo_get( &rx_buffer->uart_buffer, &byte ) ) {
    if ( rx_buffer->length < RN2903_BUFFER_LENGTH ) {
      rx_buffer->buffer[rx_buffer->length] = byte;
      rx_buffer->length++;
    } else {
      buffer_overrun = 1;
      break;
    }
  }
  return buffer_overrun;
}

#define NEXT_STATE( state )                                                                                            \
  com->next_status = state;                                                                                            \
  _rn2903_debug( "nuevo estado " #state );                                                                             \
  if ( !com->notification_cb ) {                                                                                       \
    _rn2903_error( "no hay callback de notificacion" );                                                                \
  } else {                                                                                                             \
    com->notification_cb( com->notification_ctx );                                                                     \
  }

#define DIE( msg )                                                                                                     \
  com->init_done = 1;                                                                                                  \
  _rn2903_error( msg );                                                                                                \
  NEXT_STATE( rn2903_state_dead );

void _rn2903_decode_receive_data( struct rn2903_com_t *com ) {
  _rn2903_debug( "recibiendo datos" );
  int16_t next_length = com->rx_buffer.send_ix;
  int16_t done = 0;
  for ( ; com->rx_buffer.send_ix + 1 < com->rx_buffer.length && !done; com->rx_buffer.send_ix += 2 ) {
    if ( com->rx_buffer.buffer[com->rx_buffer.send_ix] == '\r' &&
         com->rx_buffer.buffer[com->rx_buffer.send_ix + 1] == '\n' ) {
      done = 1;
    } else {
      uint8_t byte = _rn2903_digits_to_byte( com->rx_buffer.buffer[com->rx_buffer.send_ix],
                                             com->rx_buffer.buffer[com->rx_buffer.send_ix + 1] );
      com->rx_buffer.buffer[next_length] = byte;
      next_length++;
    }
  }

  // si quedó un byte sin convertir lo traemos hacia los datos convertidos.
  if ( com->rx_buffer.send_ix < com->rx_buffer.length ) {
    com->rx_buffer.buffer[next_length] = com->rx_buffer.buffer[com->rx_buffer.send_ix];
    com->rx_buffer.length = next_length + 1;
  } else {
    com->rx_buffer.length = next_length;
  }
  com->rx_buffer.send_ix = next_length;

  if ( done ) {
    com->rx_callback_status = rn2903_rx_ok;
    NEXT_STATE( rn2903_state_rx_callback );
  }
}

// IDLE callbacks.
void _rn2903_uart_idle_write_callback( void *ctx, enum uart_tx_status_e uart_status ) {
  struct rn2903_com_t *com = ctx;
  if ( uart_status == uart_tx_fifo_empty && com->expecting_fifo_empty_cb ) {
    com->expecting_fifo_empty_cb = 0;
  } else {
    DIE( "fifo de tx vacia cuando estabamos idle y no la esperabamos" );
  }
}

void _rn2903_uart_idle_reply_callback( void *ctx, enum uart_rx_status_e uart_status ) {}

// Tx callbacks

void _rn2903_send_uart_rx_callback( void *ctx, enum uart_rx_status_e uart_status ) {
  struct rn2903_com_t *com = ctx;

  int buffer_overrun = 0;
  if ( uart_status == uart_rx_ok ) {
    buffer_overrun = _rn2903_xfer_from_uart_to_buffer( &com->rx_buffer );
  }

  switch ( com->status ) {
  case rn2903_state_tx_cmd: {
    DIE( "recibimos un mensaje cuando estabamos enviando comando TX" );
    break;
  }

  case rn2903_state_tx_data: {
    switch ( uart_status ) {
    case uart_rx_ok: {
      if ( _rn2903_is_ok_reply( &com->rx_buffer ) ) {
        _rn2903_debug( "comando de TX aceptado" );
        memmove( com->rx_buffer.buffer, &com->rx_buffer.buffer[4], com->rx_buffer.length - 4 );
        com->rx_buffer.length -= 4;
        NEXT_STATE( rn2903_state_tx_wait_reply );
      } else if ( _rn2903_is_invalid_param_reply( &com->rx_buffer ) ) {
        DIE( "parametro invalido" );
      } else if ( _rn2903_is_busy_reply( &com->rx_buffer ) ) {
        DIE( "dispositivo ocupado" );
      } else {
        if ( _rn2903_has_CRLF_ending( &com->rx_buffer ) ) {
          _rn2903_debug_print_crlf( &com->rx_buffer );
          DIE( "mensaje desconocido como primera respuesta a comando TX" );
        }
      }
      break;
    }
    case uart_rx_timeout: {
      DIE( "timeout de rx esperando respuesta de TX" );
      break;
    }
    case uart_rx_buffer_overrun: {
      DIE( "buffer overrun de rx cuando estabamos enviando comando TX" );
      break;
    }
    case uart_rx_error: {
      DIE( "error de uart rx cuando estabamos enviando comando TX" );
      break;
    }
    }
    break;
  }

  case rn2903_state_tx_wait_reply: {
    switch ( uart_status ) {
    case uart_rx_ok: {
      if ( _rn2903_is_reply_tx_ok_reply( &com->rx_buffer ) ) {
        _rn2903_debug( "transmision completa" );
        com->tx_callback_status = rn2903_tx_ok;
        NEXT_STATE( rn2903_state_tx_callback );
      } else if ( _rn2903_is_radio_err_reply( &com->rx_buffer ) ) {
        _rn2903_debug( "radio timeout cuando transmitiamos" );
        com->tx_callback_status = rn2903_tx_error;
        NEXT_STATE( rn2903_state_tx_callback );
      } else {
        if ( _rn2903_has_CRLF_ending( &com->rx_buffer ) ) {
          _rn2903_debug_print_crlf( &com->rx_buffer );
          DIE( "tx_wait_reply: mensaje desconocido como segunda respuesta a comando TX" );
        }
      }
      break;
    }
    case uart_rx_timeout: {
      DIE( "timeout de rx esperando respuesta de TX" );
      break;
    }
    case uart_rx_buffer_overrun: {
      DIE( "buffer overrun de rx cuando estabamos esperando "
           "respuesta comando TX" );
      break;
    }
    case uart_rx_error: {
      DIE( "error de uart rx cuando estabamos esperando respuesta comando TX" );
      break;
    }
    }
    break;
  }
  default: {
    DIE( "llamado a callback de send uart tx cuando estamos en otro estado" );
  }
  }
}

void _rn2903_send_uart_write_callback( void *ctx, enum uart_tx_status_e status ) {
  struct rn2903_com_t *com = ctx;

  switch ( com->status ) {
  case rn2903_state_tx_cmd: {
    switch ( status ) {
    case uart_tx_fifo_empty: {
      NEXT_STATE( rn2903_state_tx_data );
      break;
    }
    case uart_tx_error: {
      DIE( "error de uart cuando estabamos enviando comando de TX" );
      break;
    }
    }
    break;
  }
  case rn2903_state_tx_data: {
    switch ( status ) {
    case uart_tx_fifo_empty: {
      _rn2903_debug( "fifo de tx de uart vacia" );
      com->expecting_fifo_empty_cb = 0;
      if ( com->tx_buffer.send_ix < com->tx_buffer.length ) {
        int16_t fifo_space = uart_fifo_empty_space( &com->tx_buffer.uart_buffer );
        if ( fifo_space >= 2 ) {
          _rn2903_debug( "fifo de tx de uart vacia, llenando" );
          while ( com->tx_buffer.send_ix < com->tx_buffer.length && fifo_space >= 2 ) {
            uint8_t data = com->tx_buffer.buffer[com->tx_buffer.send_ix];
            uint8_t hex[2];
            com->tx_buffer.send_ix++;
            _rn2903_byte_to_hex( data, hex );
            fifo_space -= 2;
            if ( uart_fifo_put( &com->tx_buffer.uart_buffer, hex, 2 ) != 2 ) {
              _rn2903_debug( "no pudimos poner 2 caracteres en la fifo de tx" );
              fifo_space = 0;
            }
          }
          if ( com->tx_buffer.send_ix == com->tx_buffer.length && fifo_space >= 2 ) {
            uint8_t ending[2] = "\r\n";
            if ( uart_fifo_put( &com->tx_buffer.uart_buffer, ending, 2 ) != 2 ) {
              _rn2903_debug( "no pudimos poner 2 caracteres en la fifo de tx" );
              fifo_space = 0;
            } else {
              com->tx_buffer.send_ix += 2;
            }
          }
        }
        // reiniciamos la lectura con el mismo callback y contexto.
        uart_begin_write( com->uart, &com->tx_buffer.uart_buffer, _rn2903_send_uart_write_callback, ctx );
        com->expecting_fifo_empty_cb = 1;
      }
      break;
    }
    case uart_tx_error: {
      DIE( "error de tx uart cuando estabamos enviando datos de TX" );
      break;
    }
    }
    break;
  }
  case rn2903_state_tx_wait_reply: {
    switch ( status ) {
    case uart_tx_fifo_empty: {
      if ( com->expecting_fifo_empty_cb ) {
        com->expecting_fifo_empty_cb = 0;
      } else {
        _rn2903_debug( "fifo de tx de uart vacia cuando estabamos esperando respuesta "
                       "de comando de TX" );
      }
      break;
    }
    case uart_tx_error: {
      DIE( "error de tx de uart cuando estabamos esperando respuesta "
           "de comando de TX" );
      break;
    }
    }
    break;
  }
  default: {
    DIE( "callback de tx de uart en estado no relacionado" );
    break;
  }
  }
}

// receive callbacks

void _rn2903_receive_uart_rx_callback( void *ctx, enum uart_rx_status_e uart_status ) {
  struct rn2903_com_t *com = ctx;

  int buffer_overrun = _rn2903_xfer_from_uart_to_buffer( &com->rx_buffer );

  /* _rn2903_debug("rurc"); */

  switch ( com->status ) {
  case rn2903_state_rx_cmd: {
    switch ( uart_status ) {
    case uart_rx_ok: {
      if ( _rn2903_is_ok_reply( &com->rx_buffer ) ) {
        _rn2903_debug( "comando de RX aceptado" );
        memmove( com->rx_buffer.buffer, &com->rx_buffer.buffer[4], com->rx_buffer.length - 4 );
        com->rx_buffer.length -= 4;
        NEXT_STATE( rn2903_state_rx_wait_reply );
      } else if ( _rn2903_is_invalid_param_reply( &com->rx_buffer ) ) {
        DIE( "parametro invalido" );
      } else if ( _rn2903_is_busy_reply( &com->rx_buffer ) ) {
        DIE( "dispositivo ocupado" );
      } else {
        if ( _rn2903_has_CRLF_ending( &com->rx_buffer ) ) {
          _rn2903_debug_print_crlf( &com->rx_buffer );
          DIE( "mensaje desconocido como primera respuesta a comando RX" );
        }
      }
      break;
    }
    case uart_rx_timeout: {
      DIE( "timeout de rx esperando respuesta de RX" );
      break;
    }
    case uart_rx_buffer_overrun: {
      DIE( "buffer overrun de rx cuando estabamos enviando comando RX" );
      break;
    }
    case uart_rx_error: {
      DIE( "error de uart rx cuando estabamos enviando comando RX" );
      break;
    }
    }
    break;
  }

  case rn2903_state_rx_wait_reply: {
    switch ( uart_status ) {
    case uart_rx_ok: {
      if ( _rn2903_is_reply_rx_data_reply( &com->rx_buffer ) ) {
        _rn2903_debug( "recepción de datos iniciada" );
        int16_t prefix_length = sizeof( radio_rx_prefix ) - 1;
        memmove( com->rx_buffer.buffer, &com->rx_buffer.buffer[prefix_length], com->rx_buffer.length - prefix_length );
        com->rx_buffer.length -= prefix_length;
        NEXT_STATE( rn2903_state_rx_decode_reply );
      } else if ( _rn2903_is_radio_err_reply( &com->rx_buffer ) ) {
        com->rx_buffer.length = 0;
        _rn2903_debug( "radio timeout en recepción" );
        com->rx_callback_status = rn2903_rx_timeout;
        NEXT_STATE( rn2903_state_rx_callback );
      } else {
        if ( _rn2903_has_CRLF_ending( &com->rx_buffer ) ) {
          _rn2903_debug_print_crlf( &com->rx_buffer );
          DIE( "rx_wait_reply: mensaje desconocido como segunda respuesta a comando RX" );
        }
      }
      break;
    }
    case uart_rx_timeout: {
      DIE( "timeout de uart rx esperando respuesta de TX" );
      break;
    }
    case uart_rx_buffer_overrun: {
      DIE( "buffer overrun de rx cuando estabamos esperando "
           "segunda respuesta comando RX" );
      break;
    }
    case uart_rx_error: {
      DIE( "error de uart rx cuando estabamos esperando respuesta comando RX" );
      break;
    }
    }
    break;
  }

  case rn2903_state_rx_decode_reply: {
    switch ( uart_status ) {
    case uart_rx_ok: {
      _rn2903_decode_receive_data( com );
      break;
    }
    case uart_rx_timeout: {
      DIE( "timeout de rx esperando segunda respueta de RX" );
      break;
    }
    case uart_rx_buffer_overrun: {
      DIE( "buffer overrun de uart rx cuando estabamos esperando "
           "segunda respuesta comando RX" );
      break;
    }
    case uart_rx_error: {
      DIE( "error de uart rx cuando estabamos esperando segunda respuesta comando RX" );
      break;
    }
    }
    break;
  }
  default: {
    DIE( "llamado a callback de receive uart rx cuando estamos en otro estado" );
  }
  }
}

void _rn2903_receive_uart_write_callback( void *ctx, enum uart_tx_status_e status ) {

  struct rn2903_com_t *com = ctx;

  switch ( com->status ) {
  case rn2903_state_rx_cmd: {
    switch ( status ) {
    case uart_tx_fifo_empty: {
      if ( com->expecting_fifo_empty_cb ) {
        com->expecting_fifo_empty_cb = 0;
      } else {
        _rn2903_debug( "fifo de tx de uart vacia cuando ya estaba vacía" );
      }
      break;
    }
    case uart_tx_error: {
      DIE( "error de uart cuando estabamos enviando comando de RX" );
      break;
    }
    }
    break;
  }
  case rn2903_state_rx_wait_reply: {
    switch ( status ) {
    case uart_tx_fifo_empty: {
      if ( com->expecting_fifo_empty_cb ) {
        com->expecting_fifo_empty_cb = 0;
      } else {
        _rn2903_debug( "fifo de tx de uart vacia cuando estabamos esperando respuesta "
                       "de comando de RX" );
      }
      break;
    }
    case uart_tx_error: {
      DIE( "error de tx de uart cuando estabamos esperando respuesta "
           "de comando de RX" );
      break;
    }
    }
    break;
  }
  default: {
    DIE( "callback de receive de uart tx en estado no relacionado" );
    break;
  }
  }
}

// cmd callbacks

void _rn2903_uart_cmd_reply_callback( void *ctx, enum uart_rx_status_e uart_status ) {
  struct rn2903_com_t *com = ctx;

  int buffer_overrun = 0;
  if ( uart_status == uart_rx_ok ) {
    buffer_overrun = _rn2903_xfer_from_uart_to_buffer( &com->rx_buffer );
  }

  switch ( com->status ) {
  case rn2903_state_cmd_issue:
  case rn2903_state_cmd_reply: {
    switch ( uart_status ) {
    case uart_rx_ok: {
      if ( _rn2903_has_CRLF_ending( &com->rx_buffer ) ) {
        _rn2903_debug( "reset reply: " );
        _rn2903_debug_print_crlf( &com->rx_buffer);
        NEXT_STATE( rn2903_state_cmd_callback );
      }
      break;
    }
    case uart_rx_timeout: {
      DIE( "timeout de rx esperando respuesta de comando" );
      break;
    }
    case uart_rx_buffer_overrun: {
      DIE( "buffer overrun de rx cuando estabamos esperando respuesta de "
           "comando" );
      break;
    }
    case uart_rx_error: {
      DIE( "error de uart rx cuando estabamos esperando respuesta de comando" );
      break;
    } break;
    }
    break;
  }

  default: {
    DIE( "llamado a callback de cmd uart rx cuando estamos en otro estado" );
  }
  }
}

void _rn2903_uart_cmd_write_callback( void *ctx, enum uart_tx_status_e status ) {
  struct rn2903_com_t *com = ctx;

  switch ( com->status ) {
  case rn2903_state_cmd_issue: {
    switch ( status ) {
    case uart_tx_fifo_empty: {
      com->expecting_fifo_empty_cb = 0;
      NEXT_STATE( rn2903_state_cmd_reply );
      break;
    }
    case uart_tx_error: {
      DIE( "error de uart cuando estabamos enviando comando" );
      break;
    }
    }
    break;
  }

  case rn2903_state_cmd_reply: {
    switch ( status ) {
    case uart_tx_fifo_empty: {
      com->expecting_fifo_empty_cb = 0;
      break;
    }
    case uart_tx_error: {
      DIE( "error de tx de uart cuando estabamos esperando respuesta comando" );
      break;
    }
    }
    break;
  }

  case rn2903_state_idle: {
    break;
  }

  default: {
    if ( status == uart_tx_fifo_empty && com->expecting_fifo_empty_cb ) {
      com->expecting_fifo_empty_cb = 0;
    } else {
      DIE( "callback de comando para tx de uart en estado no relacionado" );
      break;
    }
  }
  }
}

void _rn2903_handle_new_state( struct rn2903_com_t *com ) {
  if ( com->next_status == com->status ) {
    return;
  }
  com->status = com->next_status;

  _rn2903_debug("nuevo estado");

  // Para transmitir iniciamos la lectura en la uart primero para detectar
  // cualquier respuesta que no estemos esperando y para evitar la condición
  // de carrera con la respuesta del comando.

  switch ( com->status ) {
  case rn2903_state_tx_cmd: {
    uart_begin_readline( com->uart, &com->rx_buffer.uart_buffer, 2, &_rn2903_send_uart_rx_callback, com, 10000 );
    // Escribimos solamente la primer parte del comando.
    uint8_t cmd[9] = "radio tx ";
    uart_fifo_put( &com->tx_buffer.uart_buffer, cmd, sizeof( cmd ) );
    uart_begin_write( com->uart, &com->tx_buffer.uart_buffer, &_rn2903_send_uart_write_callback, com );
    com->expecting_fifo_empty_cb = 1;
    break;
  }
  case rn2903_state_tx_data: {
    uart_begin_write( com->uart, &com->tx_buffer.uart_buffer, &_rn2903_send_uart_write_callback, com );
    com->expecting_fifo_empty_cb = 1;
    break;
  }
  case rn2903_state_tx_wait_reply: {
    _rn2903_send_uart_rx_callback( com, uart_rx_ok );
    break;
  }
  case rn2903_state_idle: {
    if ( !com->init_cb_called && com->init_done ) {
      com->init_cb_called = 1;
      if ( com->init_cb ) {
        com->init_cb( com->init_cb_ctx, rn2903_init_ok );
      }
    }

    break;
  }
  case rn2903_state_rx_cmd: {
    uart_begin_readline( com->uart, &com->rx_buffer.uart_buffer, 2, &_rn2903_receive_uart_rx_callback, com, 10000 );
    // Escribimos el
    char cmd[] = "radio rx ";
    char window[] = "00000";
    uint8_t ending[2] = "\r\n";
    int window_digits = _rn2903_rx_window_size_str( com->lora_symbols, window );
    uart_fifo_put( &com->tx_buffer.uart_buffer, (uint8_t *)cmd, sizeof( cmd ) - 1 );
    uart_fifo_put( &com->tx_buffer.uart_buffer, (uint8_t *)window, window_digits );
    uart_fifo_put( &com->tx_buffer.uart_buffer, ending, 2 );
    uart_begin_write( com->uart, &com->tx_buffer.uart_buffer, &_rn2903_receive_uart_write_callback, com );
    com->expecting_fifo_empty_cb = 1;
    break;
  }
  case rn2903_state_rx_wait_reply: {
    _rn2903_receive_uart_rx_callback( com, uart_rx_ok );
    break;
  }
  case rn2903_state_rx_decode_reply: {
    _rn2903_receive_uart_rx_callback( com, uart_rx_ok );
    // procesamos datos que ya hayamos recibido.
    _rn2903_decode_receive_data( com );
    break;
  }
  case rn2903_state_dead: {
    if ( !com->init_cb_called && com->init_done ) {
      com->init_cb_called = 1;
      if ( com->init_cb ) {
        com->init_cb( com->init_cb_ctx, rn2903_init_err );
      }
    }
    if ( com->cmd_callback ) {
      com->cmd_callback( com->cmd_callback_ctx, rn2903_cmd_error );
      com->cmd_callback = 0;
    }
    if ( com->rx_callback ) {
      com->rx_callback( com->rx_callback_ctx, rn2903_rx_error, com->rx_buffer.buffer, com->rx_buffer.length );
      com->rx_callback = 0;
    }
    if ( com->tx_callback ) {
      com->tx_callback( com->tx_callback_ctx, rn2903_tx_error );
      com->tx_callback = 0;
    }
    uart_close( com->uart );
    break;
  }
  case rn2903_state_cmd_issue: {
    uart_begin_readline( com->uart, &com->rx_buffer.uart_buffer, 1, &_rn2903_uart_cmd_reply_callback, com, 1000 );
    if ( uart_fifo_put( &com->tx_buffer.uart_buffer, (uint8_t *)&com->tx_buffer.buffer, com->tx_buffer.length ) !=
         com->tx_buffer.length ) {
      _rn2903_error( "comando no entro en fifo tx de uart" );
      com->cmd_callback_status = rn2903_cmd_too_long;
      NEXT_STATE( rn2903_state_cmd_callback );
    } else {
      uart_begin_write( com->uart, &com->tx_buffer.uart_buffer, &_rn2903_uart_cmd_write_callback, com );
      com->expecting_fifo_empty_cb = 1;
    }
    break;
  }
  case rn2903_state_cmd_reply: {
    _rn2903_uart_cmd_reply_callback( com, uart_rx_ok );
    break;
  }
  case rn2903_state_cmd_callback: {
    NEXT_STATE( rn2903_state_idle );
    rn2903_cmd_callback_fn cb = com->cmd_callback;
    com->cmd_callback = 0;
    if ( cb ) {
      cb( com->cmd_callback_ctx, com->cmd_callback_status );
    } else {
      _rn2903_debug( "no hay callback de comandos" );
    }
    com->rx_buffer.length = 0;
    break;
  }
  case rn2903_state_rx_callback: {
    NEXT_STATE( rn2903_state_idle );
    rn2903_rx_callback_fn cb = com->rx_callback;
    com->rx_callback = 0;
    if ( cb ) {
      cb( com->rx_callback_ctx, com->rx_callback_status, com->rx_buffer.buffer, com->rx_buffer.length );
    } else {
      _rn2903_debug( "no hay callback de receive" );
    }
    break;
  }
  case rn2903_state_tx_callback: {
    NEXT_STATE( rn2903_state_idle );
    rn2903_tx_callback_fn cb = com->tx_callback;
    com->tx_callback = 0;
    if ( cb ) {
      cb( com->tx_callback_ctx, com->tx_callback_status );
    } else {
      _rn2903_debug( "no hay callback de send" );
    }

    break;
  }
  }
}

// enviamos reset dos veces para cortar cualquier comando que haya quedado
// trunco. la primera respuesta será "invalid_param" y la siguiente reseteará.
const char *RESET_CMDS[] = {"sys reset\r\n",       "sys reset\r\n",        "mac pause\r\n",
                            "radio set pwr 10\r\n", "radio set sf sf7\r\n", "radio set prlen 35\r\n"};

void _rn2903_reset_commands_cb( void *ctx, rn2903_cmd_status_e status ) {
  struct rn2903_com_t *com = ctx;
  switch ( status ) {
  case rn2903_cmd_ok: {
    if ( com->reset_cmd_ix < (int)( sizeof( RESET_CMDS ) / sizeof( RESET_CMDS[0] ) ) ) {
      int len = strlen( RESET_CMDS[com->reset_cmd_ix] );
      rn2903_issue_cmd( com, _rn2903_reset_commands_cb, com, RESET_CMDS[com->reset_cmd_ix], len );
      com->reset_cmd_ix++;
    } else {
      com->init_done = 1;
      _rn2903_debug( "reset completo" );
    }
    break;
  }
  case rn2903_cmd_error: {
    com->init_done = 1;
    _rn2903_error( "error en un comando de reseteo" );
    break;
  }
  case rn2903_cmd_too_long: {
    com->init_done = 1;
    _rn2903_error( "un comando de reseteo era demasiado largo" );
    break;
  }
  }
}

/**
 *Implementación de funciones públicas.
 */

int rn2903_init_com( struct rn2903_com_t *com, uart_handle_t uart, notification_callback_fn notification_cb,
                     void *notification_ctx, rn2903_init_callback_fn init_cb, void *init_cb_ctx ) {
  memset( com, 0, sizeof( *com ) );
  uart_fifo_init( &com->rx_buffer.uart_buffer );
  uart_fifo_init( &com->tx_buffer.uart_buffer );
  com->status = rn2903_state_idle;
  com->next_status = rn2903_state_idle;
  com->uart = uart;
  com->notification_cb = notification_cb;
  com->notification_ctx = notification_ctx;
  com->init_cb = init_cb;
  com->init_cb_ctx = init_cb_ctx;
  // iniciamos el proceso de reseteo.
  _rn2903_reset_commands_cb( com, rn2903_cmd_ok );
  return 0;
}

int rn2903_send( struct rn2903_com_t *com, rn2903_tx_callback_fn callback, void *context, const uint8_t *msg,
                 int16_t size ) {
  // Validamos inputs
  if ( com->status != rn2903_state_idle && com->next_status != rn2903_state_idle ) {
    _rn2903_debug( "no podemos transmitir, no estamos idle" );
    return -1;
  }
  if ( size > RN2903_BUFFER_LENGTH ) {
    _rn2903_debug( "no podemos transmitir mas que el maximo de bytes" );
    return -1;
  }

  _rn2903_txrx_buffer_init( &com->rx_buffer );
  _rn2903_txrx_buffer_init( &com->tx_buffer );
  memcpy( com->tx_buffer.buffer, msg, size );
  com->tx_buffer.length = size;
  uart_fifo_init( &com->tx_buffer.uart_buffer );
  com->tx_callback = callback;
  com->tx_callback_ctx = context;
  NEXT_STATE( rn2903_state_tx_cmd );
  _rn2903_handle_new_state( com );
  return 0;
}

int rn2903_issue_cmd( struct rn2903_com_t *com, rn2903_cmd_callback_fn callback, void *context, const char *msg,
                      int16_t size ) {
  // Validamos inputs
  // pequeño hack para resetear.
  if ( com->status != rn2903_state_idle && com->next_status != rn2903_state_idle &&
       callback != _rn2903_reset_commands_cb ) {
    _rn2903_debug( "no podemos transmitir, no estamos idle" );
    return -1;
  }
  if ( size > UART_FIFO_LENGTH ) {
    _rn2903_debug( "no podemos transmitir mas que el maximo de bytes que entra "
                   "en la fifo de tx" );
    return -1;
  }

  memcpy( com->tx_buffer.buffer, msg, size );
  com->tx_buffer.length = size;
  com->tx_buffer.send_ix = 0;
  uart_fifo_init( &com->tx_buffer.uart_buffer );
  com->cmd_callback = callback;
  com->cmd_callback_ctx = context;
  NEXT_STATE( rn2903_state_cmd_issue );
  _rn2903_handle_new_state( com );
  return 0;
}

int rn2903_receive( struct rn2903_com_t *com, uint16_t lora_symbols, rn2903_rx_callback_fn callback, void *context ) {
  if ( com->status != rn2903_state_idle && com->next_status != rn2903_state_idle ) {
    _rn2903_debug( "no podemos recibir, no estamos idle" );
    return -1;
  }

  _rn2903_txrx_buffer_init( &com->rx_buffer );
  _rn2903_txrx_buffer_init( &com->tx_buffer );
  uart_fifo_init( &com->tx_buffer.uart_buffer );
  com->rx_callback = callback;
  com->rx_callback_ctx = context;
  com->lora_symbols = lora_symbols;
  NEXT_STATE( rn2903_state_rx_cmd );
  _rn2903_handle_new_state( com );
  return 0;
}

int rn2903_run_one_iteration( struct rn2903_com_t *com ) {
  _rn2903_handle_new_state( com );
  return 1;
}

int rn2903_init_done( struct rn2903_com_t *com ) { return com->init_done && com->init_cb_called; }

int rn2903_is_dead( struct rn2903_com_t *com ) { return com->status == rn2903_state_dead; }
