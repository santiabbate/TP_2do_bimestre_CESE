/**
 * @file datalink.h
 * @author Nicolás Bertolo
 * @brief Datalink 
 * @version 0.1
 * @date 2019-11-06
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _DATALINK_H
#define _DATALINK_H

#include "rn2903.h"
#include "timer.h"

#ifdef __cplusplus
extern "C" {
#endif
typedef uint16_t lora_addr_t;

/** Enum con los resultados posibles de inicialización  */
enum lora_datalink_init_result_e { lora_datalink_init_ok = 0, lora_datalink_init_err = 1 };

/** Enum con los resultados posibles de configuración  */
enum lora_datalink_conf_result_e { lora_datalink_conf_ok = 0, lora_datalink_conf_err = 1 };

/** Enum con los resultados posibles de transmisión  */
enum lora_datalink_tx_result_e { lora_datalink_tx_ok = 0, lora_datalink_tx_no_ack = 1, lora_datalink_tx_err = 2 };

/** Enum con los resultados posibles de recepción  */
  enum lora_datalink_rx_result_e { lora_datalink_rx_ok = 0, lora_datalink_rx_timeout = 1, lora_datalink_rx_err = 2 };

/** Definiciones de tipos de funciones para callback */
typedef void ( *lora_datalink_conf_callback_fn )( void *, enum lora_datalink_conf_result_e );
typedef void ( *lora_datalink_init_callback_fn )( void *, enum lora_datalink_init_result_e );
typedef void ( *lora_datalink_tx_callback_fn )( void *, enum lora_datalink_tx_result_e );
typedef void ( *lora_datalink_rx_callback_fn )( void *, enum lora_datalink_rx_result_e, const uint8_t *buffer,
                                                int16_t size, uint8_t seq_number, lora_addr_t src );

#ifdef __cplusplus
}

#ifdef SWIG
%feature("director") lora_datalink_conf_cb_wrapper;
%constant void call_lora_datalink_conf_cb_wrapper(void * ctx, lora_datalink_conf_result_e status);
%feature("director") lora_datalink_init_cb_wrapper;
%constant void call_lora_datalink_init_cb_wrapper(void * ctx, lora_datalink_init_result_e status);
%feature("director") lora_datalink_tx_cb_wrapper;
%constant void call_lora_datalink_tx_cb_wrapper(void * ctx, lora_datalink_tx_result_e status);
%feature("director") lora_datalink_rx_cb_wrapper;
%constant void call_lora_datalink_rx_cb_wrapper(void * ctx, lora_datalink_rx_result_e status, const uint8_t *buffer, int16_t size, uint8_t seq_number, lora_addr_t src);
#endif

struct lora_datalink_conf_cb_wrapper {
  virtual ~lora_datalink_conf_cb_wrapper() {}
  virtual void operator()( lora_datalink_conf_result_e ) = 0;
};

struct lora_datalink_init_cb_wrapper {
  virtual ~lora_datalink_init_cb_wrapper() {}
  virtual void operator()( lora_datalink_init_result_e ) = 0;
};

struct lora_datalink_tx_cb_wrapper {
  virtual ~lora_datalink_tx_cb_wrapper() {}
  virtual void operator()( lora_datalink_tx_result_e ) = 0;
};

struct lora_datalink_rx_cb_wrapper {
  virtual ~lora_datalink_rx_cb_wrapper() {}
  virtual void operator()( lora_datalink_rx_result_e, const uint8_t *buffer, int16_t size, uint8_t seq_number,
                           lora_addr_t src ) = 0;
};

extern "C" {

inline void call_lora_datalink_conf_callback( lora_datalink_conf_callback_fn fn, void *ctx,
                                              lora_datalink_conf_result_e status ) {
  fn( ctx, status );
}

inline void call_lora_datalink_conf_cb_wrapper( void *ctx, lora_datalink_conf_result_e status ) {
  auto *cb = (lora_datalink_conf_cb_wrapper *)ctx;
  ( *cb )( status );
}

inline void call_lora_datalink_init_callback( lora_datalink_init_callback_fn fn, void *ctx,
                                              lora_datalink_init_result_e status ) {
  fn( ctx, status );
}

inline void call_lora_datalink_init_cb_wrapper( void *ctx, lora_datalink_init_result_e status ) {
  auto *cb = (lora_datalink_init_cb_wrapper *)ctx;
  ( *cb )( status );
}

inline void call_lora_datalink_tx_callback( lora_datalink_tx_callback_fn fn, void *ctx,
                                            lora_datalink_tx_result_e status ) {
  fn( ctx, status );
}

inline void call_lora_datalink_tx_cb_wrapper( void *ctx, lora_datalink_tx_result_e status ) {
  auto *cb = (lora_datalink_tx_cb_wrapper *)ctx;
  ( *cb )( status );
}

inline void call_lora_datalink_rx_callback( lora_datalink_rx_callback_fn fn, void *ctx,
                                            lora_datalink_rx_result_e status, const uint8_t *buffer, int16_t size,
                                            uint8_t seq_number, lora_addr_t src ) {
  fn( ctx, status, buffer, size, seq_number, src );
}

inline void call_lora_datalink_rx_cb_wrapper( void *ctx, lora_datalink_rx_result_e status, const uint8_t *buffer,
                                              int16_t size, uint8_t seq_number, lora_addr_t src ) {
  auto *cb = (lora_datalink_rx_cb_wrapper *)ctx;
  ( *cb )( status, buffer, size, seq_number, src );
}
#endif

/** 
 * @brief Estructura de configuración del link
 * 
 */
typedef struct {
  /** Configuración de frecuencia */
  int32_t freq;
  /** Potencia de transmisión */  
  int16_t tx_power;
  uint8_t CR;
  uint8_t SF;
  int32_t BW;
  int16_t preamble_symbol_length;
  /** Low data rate optimization */
  int8_t ldro;
  int8_t header_enabled;

  int16_t num_devices;
  int32_t rx_period_ms;

  int8_t _max_length;
} lora_link_conf_t;

enum lora_datalink_status_e {
  lora_datalink_status_init,
  lora_datalink_status_idle,
  lora_datalink_status_dead,

  lora_datalink_status_tx_init,
  lora_datalink_status_wait_tx_ack,
  lora_datalink_status_tx_callback,

  lora_datalink_status_rx_init,
  lora_datalink_status_rx_open_window,
  lora_datalink_status_rx_send_ack,
  lora_datalink_status_rx_callback
};

#define DATALINK_SEQ_NUMBER_WIDTH 3

typedef struct {
  lora_addr_t src;
  lora_addr_t dst;
  uint8_t key;
  uint8_t is_ack : 1;
  uint8_t is_first_seq : 1;
  uint8_t seq_number : DATALINK_SEQ_NUMBER_WIDTH;
} lora_datalink_frame_header_t;

typedef struct {
  lora_datalink_frame_header_t header;
  uint8_t msg[RN2903_MAX_MSG_LENGTH - 6];
} lora_datalink_frame_t;

#if defined(__cplusplus)
static_assert( sizeof( lora_datalink_frame_t ) == RN2903_MAX_MSG_LENGTH, "datalink frame tamaño erroneo" );
#endif

struct lora_datalink_t {
  enum lora_datalink_status_e status;
  enum lora_datalink_status_e next_status;

  struct rn2903_com_t com;
  lora_link_conf_t link_conf;
  lora_addr_t address;
  timer_handle_t timer;

  notification_callback_fn notification_cb;
  void *notification_ctx;

  enum lora_datalink_tx_result_e tx_result;
  lora_datalink_tx_callback_fn tx_cb;
  void *tx_cb_ctx;
  lora_addr_t dst;
  int8_t seq_number;
  int retries;

  enum lora_datalink_rx_result_e rx_result;
  lora_datalink_rx_callback_fn rx_cb;
  void *rx_cb_ctx;
  lora_datalink_frame_header_t ack_header;
  lora_addr_t rx_src;

  enum lora_datalink_init_result_e init_result;
  lora_datalink_init_callback_fn init_cb;
  void *init_cb_ctx;
  int init_done;

  enum lora_datalink_conf_result_e conf_result;
  lora_datalink_conf_callback_fn conf_cb;
  void *conf_cb_ctx;

  lora_datalink_frame_t frame;
  int16_t msg_size;
};

/**
 * @brief Inicializa un datalink con la UART.
 * 
 * @param datalink Puntero a estructura de control de la capa
 * @param uart Puntero a implementación de la UART de la plataforma
 * @param timer Puntero a implementación del Timer de la plataforma
 * @param notification_cb Función de callback de notificación de la capa
 * @param notification_ctx Parámetros que recibirá el callback de notificación de la capa
 * @param cb Función de callback una vez que se inicialió la capa
 * @param init_cb_ctx Parámetros que recibirá el callback de inicialización de la capa
 * @return int 
 */
int lora_datalink_init( struct lora_datalink_t *datalink, uart_handle_t uart, timer_handle_t timer,
                        notification_callback_fn notification_cb, void *notification_ctx,
                        lora_datalink_init_callback_fn cb, void *init_cb_ctx );

/**
 * @brief Configura datalink de acuerdo a parámetros pasados
 * 
 * @param datalink 
 * @param conf 
 * @return int 
 */
int lora_datalink_configure( struct lora_datalink_t *datalink, lora_link_conf_t conf );

/** Enviar frame, re transmite si fuera necesario */
int lora_datalink_transmit( struct lora_datalink_t *datalink, lora_addr_t dst, uint8_t seq_number, uint8_t first_seq,
                            const uint8_t *data, int16_t size, lora_datalink_tx_callback_fn cb, void *ctx );

/** Recibir un frame y transmitir ACK si es para este dispositivo. */
int lora_datalink_receive( struct lora_datalink_t *datalink, uint8_t seq_number, lora_addr_t src,
                           lora_datalink_rx_callback_fn cb, void *ctx );

/** Llama a callbacks si es necesario. */
int lora_datalink_run_one_iteration( struct lora_datalink_t *dl );

/** Retorna si el datalink fue inicializado correctamente */
int lora_datalink_init_done( struct lora_datalink_t *dl );

/** Retorna si el datalink está en un estado de error irrecuperable. */
int lora_datalink_is_dead( struct lora_datalink_t *dl );

/** Retorna el máximo tamaño en bytes que puede tener un mensaje dada la configuración del enlace. */
int16_t lora_datalink_max_frame_length( struct lora_datalink_t *dl );

#ifdef __cplusplus
}
#endif
#endif
