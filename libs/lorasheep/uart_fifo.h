/**
 * @file uart_fifo.h
 * @author Nicolás Bertolo
 * @brief 
 * @version 0.1
 * @date 2019-11-06
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _UART_FIFO_H
#define _UART_FIFO_H

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef UART_FIFO_LENGTH
#define UART_FIFO_LENGTH 32
#endif

typedef struct {
  char buffer[UART_FIFO_LENGTH];
  int16_t read_index;
  int16_t write_index;
  int16_t empty_space;
} uart_fifo_t;

int uart_fifo_init(uart_fifo_t * fifo);
#ifdef SWIG
%apply uint8_t *OUTPUT{uint8_t * output};
#endif
int uart_fifo_get(uart_fifo_t * fifo, uint8_t *output);

#ifdef SWIG
%pybuffer_binary(uint8_t *buffer, int16_t size)
#endif
int16_t uart_fifo_put(uart_fifo_t *fifo, uint8_t *buffer, int16_t size);

int uart_fifo_empty(uart_fifo_t *fifo);

int16_t uart_fifo_empty_space(uart_fifo_t *fifo);

#ifdef __cplusplus
}
#endif

#endif