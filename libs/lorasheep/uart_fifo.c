#ifdef __cplusplus
extern "C" {
#endif

#include "uart_fifo.h"
#include <stdio.h>

int uart_fifo_init( uart_fifo_t *fifo ) {
  fifo->read_index = 0;
  fifo->write_index = 0;
  fifo->empty_space = UART_FIFO_LENGTH - 1;
  return 1;
}

int uart_fifo_get( uart_fifo_t *fifo, uint8_t *data ) {
  if ( uart_fifo_empty( fifo ) ) {
    return 0;
  }
  *data = fifo->buffer[fifo->read_index];

  uint8_t next = fifo->read_index + 1;
  if ( next == UART_FIFO_LENGTH ) {
    next = 0;
  }
  fifo->read_index = next;
  fifo->empty_space += 1;
  return 1;
}

int16_t uart_fifo_put( uart_fifo_t *fifo, uint8_t *data, int16_t size ) {
  int16_t returnvalue = 0;
  while ( size > 0 && fifo->empty_space > 0 ) {
    uint8_t next = fifo->write_index + 1;
    if ( next == UART_FIFO_LENGTH ) {
      next = 0;
    }

    if ( next == fifo->read_index ) {
      return returnvalue;
    }

    fifo->buffer[fifo->write_index] = *data;
    fifo->empty_space -= 1;
    fifo->write_index = next;
    returnvalue++;
    data++;
    size--;
  }

  return returnvalue;
}

int uart_fifo_empty( uart_fifo_t *fifo ) {
  if ( fifo->read_index == fifo->write_index ) {
    return 1;
  }
  return 0;
}

int16_t uart_fifo_empty_space( uart_fifo_t *fifo ) { return fifo->empty_space; }

#ifdef __cplusplus
}
#endif
