#include "datalink.h"
#include "debug_print.h"
#include <stdlib.h>
#include <string.h>

#ifdef LORA_DATALINK_DEBUG
void _datalink_error( const char *errmsg ) { printf( "DATALINK ERROR: %s\n", errmsg ); }
void _datalink_debug( const char *errmsg ) { printf( "DATALINK DEBUG: %s\n", errmsg ); }
#elif ARDUINO
#include <avr/pgmspace.h>
#define _datalink_error( msg ) debug_print( PSTR( "DATALINK ERROR" ), PSTR( msg ) )
#define _datalink_debug( msg ) debug_print( PSTR( "DATALINK DEBUG" ), PSTR( msg ) )
/* #define _datalink_error( ... ) */
/* #define _datalink_debug( ... ) */
#else
void _datalink_error( const char *errmsg ) {}
void _datalink_debug( const char *errmsg ) {}
#endif

#define NEXT_STATE( state )                                                                                            \
  dl->next_status = state;                                                                                             \
  _datalink_debug( "nuevo estado " #state );                                                                           \
  if ( !dl->notification_cb ) {                                                                                        \
    _datalink_error( "no hay callback de notificacion" );                                                              \
  } else {                                                                                                             \
    dl->notification_cb( dl->notification_ctx );                                                                       \
  }

#define DIE( msg )                                                                                                     \
  _datalink_error( msg );                                                                                              \
  NEXT_STATE( lora_datalink_status_dead );

/** Funciones privadas */
lora_addr_t _datalink_addr_hton( lora_addr_t addr ) {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
  return ( addr << 8 ) | ( addr >> 8 );
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
  return addr;
#else
#error
#endif
}

lora_addr_t _datalink_addr_ntoh( lora_addr_t addr ) { return _datalink_addr_hton( addr ); }
int32_t _datalink_ceil_div( int32_t x, int32_t y ) {
  ldiv_t res = ldiv( x, y );
  if ( res.rem > 0 ) {
    res.quot++;
  }
  return res.quot;
}

int16_t _datalink_bytes_to_symbols( lora_link_conf_t conf, int16_t bytes ) {
  int32_t num = 0;
  int32_t den = 0;
  num = 8 * bytes - 4 * conf.SF + 28 + 16 - 20 * ( 1 - conf.header_enabled );
  den = 4 * ( conf.SF - 2 * conf.ldro );
  int32_t symbols = _datalink_ceil_div( num, den );
  symbols = symbols * ( conf.CR + 4 );
  symbols = symbols > 0 ? symbols : 0;
  symbols += 8;
  symbols += conf.preamble_symbol_length;
  return symbols;
}

int16_t _datalink_packet_duration_ms( lora_link_conf_t conf, int16_t symbols ) {
  int32_t symbols_1000 = 1000 * symbols + 4250;
  ldiv_t res = ldiv( symbols_1000 * ( 1 << conf.SF ), conf.BW );
  if ( res.rem > 0 ) {
    res.quot++;
  }
  return res.quot;
}

/**
 * @brief Calcula la cantidad de bytes que pueden transmitirse en un paylod de la transmisión
 * 
 * @param conf 
 * @return int8_t Cantidad de bytes
 */
int8_t _datalink_conf_max_length( lora_link_conf_t conf ) {
  int32_t rx_window_dev_us = 1000 * ( (int32_t)conf.rx_period_ms ) / conf.num_devices;
  int32_t tsym_us = _datalink_ceil_div( ( (int32_t)1 << conf.SF ) * 1000 * 1000, conf.BW );
  int32_t tpreamble_us = _datalink_ceil_div( tsym_us * ( 1000 * (int32_t)conf.preamble_symbol_length + 4250 ), 1000 );
  int32_t nsym = ( rx_window_dev_us - tpreamble_us ) / tsym_us;

  int32_t tmp = ( nsym - 8 ) / ( conf.CR + 4 );
  tmp *= ( conf.SF - 2 * conf.ldro );
  tmp += -1 + conf.SF - 11 + 5 * ( 1 - conf.header_enabled );
  tmp /= 2;
#ifdef ARDUINO
  debug_num( PSTR( "rx_window_dev_us" ), rx_window_dev_us );
  debug_num( PSTR( "tsym_us" ), tsym_us );
  debug_num( PSTR( "tpreamble_us" ), tpreamble_us );
  debug_num( PSTR( "nsym" ), nsym );
  debug_num( PSTR( "nbytes" ), tmp );
#endif
  if ( tmp <= 0 ) {
    _datalink_error( "length negativo" );
    conf._max_length = -1;
  } else {
    conf._max_length = tmp;
    if ( conf._max_length != tmp ) {
      _datalink_error( "length fuera de rango" );
      conf._max_length = -1;
    }
  }
  return conf._max_length;
}

int16_t _datalink_tx_phase( lora_link_conf_t conf, lora_addr_t addr ) {
  int64_t rx_window_dev_us = 1000 * (int64_t)conf.rx_period_ms / conf.num_devices;
  int16_t offset = ( rx_window_dev_us * ( addr - 1 ) ) / 1000;
  return offset;
}

uint8_t _datalink_calc_xor_buffer( const uint8_t *data, int16_t size ) {
  uint8_t result = 0;
  for ( int i = 0; i < size; ++i ) {
    result ^= data[i];
  }
  return result;
}

int _datalink_frame_authenticate( const lora_datalink_frame_t *frame, int16_t size ) {
  uint8_t xor_key_buffer = _datalink_calc_xor_buffer( (const uint8_t *)frame, size );
  return xor_key_buffer == 0;
}

void _datalink_tx_send_callback( void *ctx, rn2903_tx_status_e status ) {
  struct lora_datalink_t *dl = ctx;
  if ( dl->status == lora_datalink_status_tx_init ) {
    switch ( status ) {
    case rn2903_tx_error: {
      _datalink_error( "error de transmisión por RN2903" );
      if ( dl->retries < 10 ) {
        dl->retries++;
        _datalink_debug( "retransmitiendo paquete" );
        NEXT_STATE( lora_datalink_status_tx_init );
      } else {
        _datalink_debug( "fallaron 10 retransmisiones" );
        dl->tx_result = lora_datalink_tx_no_ack;
        NEXT_STATE( lora_datalink_status_tx_callback );
      }
      NEXT_STATE( lora_datalink_status_tx_init );
      break;
    }
    case rn2903_tx_ok: {
      _datalink_debug( "paquete enviado" );
      NEXT_STATE( lora_datalink_status_wait_tx_ack );
    }
    }

  } else {
    DIE( "callback de tx de send llamada cuando no estamos en el estado correcto" );
  }
}

void _datalink_tx_receive_callback( void *ctx, rn2903_rx_status_e status, const uint8_t *buffer, int16_t size ) {
  struct lora_datalink_t *dl = ctx;
  int retry = 0;
  if ( dl->status == lora_datalink_status_wait_tx_ack ) {
    switch ( status ) {
    case rn2903_rx_ok: {
      _datalink_debug( "recepción de ACK tentativo" );
      const lora_datalink_frame_header_t *header = (void *)buffer;
      lora_addr_t header_src = _datalink_addr_ntoh( header->src );
      lora_addr_t header_dst = _datalink_addr_ntoh( header->dst );
      int is_ack = 0;
      if ( (uint32_t)size < sizeof( lora_datalink_frame_header_t ) ) {
        _datalink_debug( "paquete demasiado chico" );
      } else if ( !header->is_ack ) {
        _datalink_debug( "flag is_ack no seteada" );
      } else if ( header_src != dl->dst ) {
        _datalink_debug( "dst de tx y src de ack no coinciden" );
      } else if ( header_dst != dl->address ) {
        _datalink_debug( "src de tx y dst de ack no coinciden" );
      } else if ( header->seq_number != dl->seq_number ) {
        _datalink_debug( "seq_number no coinciden" );
      } else if ( !_datalink_frame_authenticate( (const lora_datalink_frame_t *)buffer, size ) ) {
        _datalink_debug( "paquete no autenticado" );
      } else {
        _datalink_debug( "ack aceptado" );
        is_ack = 1;
      }

      if ( is_ack ) {
        dl->tx_result = lora_datalink_tx_ok;
        NEXT_STATE( lora_datalink_status_tx_callback );
      } else {
        retry = 1;
      }
      break;
    }
    case rn2903_rx_timeout: {
      _datalink_debug( "timeout en recepción de ACK" );
      retry = 1;
      break;
    }
    case rn2903_rx_error: {
      DIE( "error de recepción por RN2903 al esperar el ACK" );
      break;
    }
    }

    if ( retry ) {
      if ( dl->retries < 10 ) {
        dl->retries++;
        _datalink_debug( "retransmitiendo paquete" );
        NEXT_STATE( lora_datalink_status_tx_init );
      } else {
        _datalink_debug( "fallaron 10 retransmisiones" );
        dl->tx_result = lora_datalink_tx_no_ack;
        NEXT_STATE( lora_datalink_status_tx_callback );
      }
    }
  } else {
    DIE( "callback de tx de send llamada cuando no estamos en el estado correcto" );
  }
}

void _datalink_rn2903_init_cb( void *ctx, rn2903_init_status_e status ) {
  struct lora_datalink_t *dl = ctx;
  switch ( status ) {
  case rn2903_init_ok: {
    _datalink_debug( "init ok" );
    NEXT_STATE( lora_datalink_status_idle );
    break;
  }
  case rn2903_init_err: {
    DIE( "init error" )
    break;
  }
  }
}

void _datalink_rx_receive_cb( void *ctx, rn2903_rx_status_e status, const uint8_t *buffer, int16_t size ) {
  struct lora_datalink_t *dl = ctx;
  switch ( status ) {
  case rn2903_rx_error: {
    DIE( "error de recepción de rn2903" );
    break;
  }
  case rn2903_rx_timeout: {
    _datalink_debug( "timeout en recepción" );
    dl->rx_result = lora_datalink_rx_timeout;
    NEXT_STATE( lora_datalink_status_rx_callback );
    break;
  }
  case rn2903_rx_ok: {
    _datalink_debug( "recepción de paquete" );
    const lora_datalink_frame_header_t *header = (void *)buffer;
    lora_addr_t header_dst = _datalink_addr_ntoh( header->dst );
    lora_addr_t header_src = _datalink_addr_hton( header->src );
    int is_accepted = 0;
    if ( (uint32_t)size < sizeof( lora_datalink_frame_header_t ) ) {
      _datalink_debug( "paquete demasiado chico" );
    } else if ( header->is_ack ) {
      _datalink_debug( "flag is_ack seteada" );
    } else if ( header_dst != dl->address ) {
#ifdef ARDUINO
      debug_num( PSTR( "header_dst" ), header_dst );
      debug_num( PSTR( "dl->address" ), dl->address );
#endif
      _datalink_debug( "addr y dst de paquete no coinciden" );
    } else if ( dl->seq_number >= 0 && header_src != dl->rx_src ) {
      _datalink_debug( "pkt src no es el deseado" );
    } else if ( dl->seq_number < 0 && !header->is_first_seq ) {
      _datalink_debug( "pkt no es el primero de la secuencia" );
    } else if ( dl->seq_number >= 0 && header->is_first_seq ) {
      _datalink_debug( "el pkt está marcado como inicio de secuencia cuando estamos recibiendo los del medio" );
    } else if ( !_datalink_frame_authenticate( (const lora_datalink_frame_t *)buffer, size ) ) {
      _datalink_debug( "paquete no autenticado" );
    } else if ( dl->seq_number >= 0 && header->seq_number > dl->seq_number ) {
      _datalink_debug( "seq_number no es el deseado" );
    } else {
      _datalink_debug( "paquete aceptado" );
      is_accepted = 1;
    }

    if ( is_accepted ) {
      memmove( &dl->frame, buffer, size );
      dl->msg_size = size - sizeof( lora_datalink_frame_header_t );
      dl->rx_result = lora_datalink_rx_ok;
      dl->ack_header = dl->frame.header;
      dl->ack_header.src = _datalink_addr_hton( dl->address );
      dl->ack_header.dst = dl->frame.header.src;
      dl->ack_header.is_ack = 1;
      dl->ack_header.key = 0;
      dl->ack_header.key =
          _datalink_calc_xor_buffer( (const uint8_t *)&dl->ack_header, sizeof( lora_datalink_frame_header_t ) );
      NEXT_STATE( lora_datalink_status_rx_send_ack );
    } else {
      _datalink_debug( "timeout en recepción de pkt esperado" );
      dl->rx_result = lora_datalink_rx_timeout;
      NEXT_STATE( lora_datalink_status_rx_callback );
    }
    break;
  }
  }
}

void _datalink_rx_timer_cb( void *ctx ) {
  struct lora_datalink_t *dl = ctx;
  if ( dl->status == lora_datalink_status_idle ) {
    timer_cancel( dl->timer );
    NEXT_STATE( lora_datalink_status_rx_open_window );
  } else {
    DIE( "callback de timer de rx init llamada en otro estado" );
  }
}

void _datalink_rx_send_ack_rn2903_cb( void *ctx, rn2903_tx_status_e status ) {
  struct lora_datalink_t *dl = ctx;
  if ( dl->status == lora_datalink_status_rx_send_ack ) {
    switch ( status ) {
    case rn2903_tx_ok: {
      int seq_numbers_equal = dl->seq_number == dl->ack_header.seq_number;
      if ( seq_numbers_equal || dl->seq_number == -1 ) {
        _datalink_debug( "ack enviado" );
        dl->rx_result = lora_datalink_rx_ok;
        NEXT_STATE( lora_datalink_status_rx_callback );
      } else {
        _datalink_debug( "ack reenviado" );
        NEXT_STATE( lora_datalink_status_rx_init );
      }

      break;
    }

    case rn2903_tx_error: {
      DIE( "error en envío de ack" );
    }
    }
  } else {
    DIE( "callback de timer de rx ack send llamada en otro estado" );
  }
}

void _datalink_rx_ack_send_timer_cb( void *ctx ) {
  struct lora_datalink_t *dl = ctx;
  if ( dl->status == lora_datalink_status_rx_send_ack ) {
    timer_cancel( dl->timer );
    int ret = rn2903_send( &dl->com, _datalink_rx_send_ack_rn2903_cb, dl, (const uint8_t *)&dl->ack_header,
                           sizeof( lora_datalink_frame_header_t ) );
    if ( ret ) {
      DIE( "transmision de ack fallida" );
    }
  } else {
    DIE( "callback de timer de rx init llamada en otro estado" );
  }
}

void _datalink_tx_timer_cb( void *ctx ) {
  struct lora_datalink_t *dl = ctx;
  timer_cancel( dl->timer );
  if ( dl->status == lora_datalink_status_tx_init ) {
    int16_t size = dl->msg_size + sizeof( lora_datalink_frame_header_t );
    int ret = rn2903_send( &dl->com, _datalink_tx_send_callback, dl, (const uint8_t *)&dl->frame, size );
    if ( ret ) {
      DIE( "comienzo de transmisión fallido" );
      return;
    }
  } else {
    DIE( "callback de timer de tx init llamada en otro estado" );
  }
}

void _datalink_tx_wait_ack_timer_cb( void *ctx ) {
  struct lora_datalink_t *dl = ctx;
  timer_cancel( dl->timer );
  if ( dl->status == lora_datalink_status_wait_tx_ack ) {
    int16_t symbols = _datalink_bytes_to_symbols( dl->link_conf, sizeof( lora_datalink_frame_header_t ) );
    int ret = rn2903_receive( &dl->com, symbols, &_datalink_tx_receive_callback, dl );
    if ( ret ) {
      DIE( "comienzo de recepción de ACK fallido" );
      return;
    }
  } else {
    DIE( "callback de timer de tx wait ack llamada en otro estado" );
  }
}

void _datalink_handle_new_state( struct lora_datalink_t *dl ) {
  if ( dl->next_status == dl->status ) {
    return;
  }
  dl->status = dl->next_status;

  switch ( dl->status ) {
  case lora_datalink_status_init: {
    break;
  }
  case lora_datalink_status_idle: {
    if ( !dl->init_done ) {
      dl->init_done = 1;
      if ( dl->init_cb ) {
        dl->init_cb( dl->init_cb_ctx, lora_datalink_init_ok );
      }
    }
    break;
  }
  case lora_datalink_status_dead: {
    if ( !dl->init_done ) {
      dl->init_done = 1;
      if ( dl->init_cb ) {
        dl->init_cb( dl->init_cb_ctx, lora_datalink_init_err );
      }
    } else {
      lora_datalink_tx_callback_fn tx_cb = dl->tx_cb;
      dl->tx_cb = 0;
      if ( tx_cb ) {
        tx_cb( dl->tx_cb_ctx, lora_datalink_tx_err );
      } else {
        _datalink_debug( "no hay callback de tx" );
      }
      lora_datalink_rx_callback_fn rx_cb = dl->rx_cb;
      dl->rx_cb = 0;
      if ( rx_cb ) {
        rx_cb( dl->rx_cb_ctx, lora_datalink_rx_err, 0, 0, 0, 0 );
      } else {
        _datalink_debug( "no hay callback de rx" );
      }
    }
    break;
  }
    // iniciamos la transmisión.
  case lora_datalink_status_tx_init: {
    timer_cancel( dl->timer );
    int16_t phase = _datalink_tx_phase( dl->link_conf, dl->address );
    timer_synch_start( dl->timer, dl->link_conf.rx_period_ms, phase, _datalink_tx_timer_cb, dl );
    break;
  }

  case lora_datalink_status_wait_tx_ack: {
    timer_cancel( dl->timer );
    int16_t phase = _datalink_tx_phase( dl->link_conf, dl->dst );
    timer_synch_start( dl->timer, dl->link_conf.rx_period_ms, phase, _datalink_tx_wait_ack_timer_cb, dl );
    break;
  }

  case lora_datalink_status_tx_callback: {
    NEXT_STATE( lora_datalink_status_idle );
    lora_datalink_tx_callback_fn cb = dl->tx_cb;
    dl->tx_cb = 0;
    if ( cb ) {
      cb( dl->tx_cb_ctx, dl->tx_result );
    } else {
      _datalink_debug( "no hay callback de tx" );
    }
    break;
  }

  case lora_datalink_status_rx_init: {
    _datalink_debug( "iniciamos timer de rx" );
    timer_cancel( dl->timer );
    int16_t phase = _datalink_tx_phase( dl->link_conf, dl->rx_src );
    timer_synch_start( dl->timer, dl->link_conf.rx_period_ms, phase, _datalink_rx_timer_cb, dl );
    NEXT_STATE( lora_datalink_status_idle );

    break;
  }

  case lora_datalink_status_rx_open_window: {
    int16_t symbols = _datalink_bytes_to_symbols( dl->link_conf, sizeof( lora_datalink_frame_t ) );
    int ret = rn2903_receive( &dl->com, symbols, _datalink_rx_receive_cb, dl );
    if ( ret ) {
      DIE( "comienzo de recepción fallido" );
    }
    break;
  }

  case lora_datalink_status_rx_send_ack: {
    _datalink_debug( "iniciamos timer de rx send ack" );
    timer_cancel( dl->timer );
    int16_t phase = _datalink_tx_phase( dl->link_conf, dl->address );
    timer_synch_start( dl->timer, dl->link_conf.rx_period_ms, phase, _datalink_rx_ack_send_timer_cb, dl );
    break;
  }

  case lora_datalink_status_rx_callback: {
    NEXT_STATE( lora_datalink_status_idle );
    lora_datalink_rx_callback_fn cb = dl->rx_cb;
    /* dl->rx_cb = 0; */
    if ( cb ) {
      lora_addr_t src = _datalink_addr_ntoh( dl->frame.header.src );
      _datalink_debug( "1" );
      cb( dl->rx_cb_ctx, dl->rx_result, dl->frame.msg, dl->msg_size, dl->frame.header.seq_number, src );
      _datalink_debug( "2" );
    } else {
      _datalink_debug( "no hay callback de rx" );
    }
    break;
  }
  }
}

/** Funciones publicas */

int lora_datalink_init( struct lora_datalink_t *datalink, uart_handle_t uart, timer_handle_t timer,
                        notification_callback_fn notification_cb, void *notification_ctx,
                        lora_datalink_init_callback_fn cb, void *init_cb_ctx ) {
  memset( datalink, 0, sizeof( struct lora_datalink_t ) );
  datalink->status = lora_datalink_status_init;
  datalink->link_conf.BW = 125000;
  datalink->link_conf.CR = 1;
  datalink->link_conf.SF = 7;
  datalink->link_conf.header_enabled = 1;
  datalink->link_conf.ldro = 0;
  datalink->link_conf.preamble_symbol_length = 35;
  datalink->link_conf.num_devices = 50;
  datalink->link_conf.rx_period_ms = 5000 * ( 1 << ( datalink->link_conf.SF - 7 ) );
  datalink->link_conf._max_length = _datalink_conf_max_length( datalink->link_conf );
  if ( datalink->link_conf._max_length < 0 || datalink->link_conf._max_length < 10 ) {
    _datalink_error( "length pequeno" );
    return -1;
  }
  if ( (uint8_t)datalink->link_conf._max_length > sizeof( datalink->frame.msg ) ) {
    _datalink_error( "length muy grande" );
    return -2;
  }
  datalink->init_cb = cb;
  datalink->init_cb_ctx = init_cb_ctx;
  datalink->notification_cb = notification_cb;
  datalink->notification_ctx = notification_ctx;
  datalink->timer = timer;
  return rn2903_init_com( &datalink->com, uart, notification_cb, notification_ctx, &_datalink_rn2903_init_cb,
                          datalink );
}

int lora_datalink_configure( struct lora_datalink_t *datalink, lora_link_conf_t conf ) { return 0; }

int lora_datalink_transmit( struct lora_datalink_t *dl, lora_addr_t dst, uint8_t seq_number, uint8_t first_seq,
                            const uint8_t *data, int16_t size, lora_datalink_tx_callback_fn cb, void *ctx ) {
  if ( dl->status == lora_datalink_status_dead ) {
    _datalink_error( "estamos dead, no podemos transmitir" );
    return -1;
  }
  if ( dl->status != lora_datalink_status_idle && dl->status != lora_datalink_status_rx_callback &&
       dl->status != lora_datalink_status_tx_callback ) {
    _datalink_error( "no estamos idle, no podemos transmitir" );
    return -2;
  }
  if ( size > dl->link_conf._max_length ) {
    _datalink_error( "tx msg demasiado largo" );
    return -3;
  }

  // preparamos el paquete
  lora_datalink_frame_t *frame = &dl->frame;
  frame->header.dst = _datalink_addr_hton( dst );
  frame->header.src = _datalink_addr_hton( dl->address );
  frame->header.is_ack = 0;
  frame->header.seq_number = seq_number;
  frame->header.is_first_seq = !!first_seq;
  frame->header.key = 0;
  memmove( frame->msg, data, size );
  frame->header.key =
      _datalink_calc_xor_buffer( (const uint8_t *)frame, size + sizeof( lora_datalink_frame_header_t ) );

  dl->tx_cb = cb;
  dl->tx_cb_ctx = ctx;
  dl->msg_size = size;
  dl->dst = dst;
  dl->seq_number = seq_number;
  dl->retries = 0;
  if ( dl->status == lora_datalink_status_idle || dl->status == lora_datalink_status_rx_init ) {
    timer_cancel( dl->timer );
    NEXT_STATE( lora_datalink_status_tx_init );
  }
  return 0;
}

int lora_datalink_receive( struct lora_datalink_t *dl, uint8_t seq_number, lora_addr_t src,
                           lora_datalink_rx_callback_fn cb, void *ctx ) {
  if ( dl->status == lora_datalink_status_dead ) {
    _datalink_error( "estamos dead, no podemos recibir" );
    return -1;
  }
  if ( dl->status != lora_datalink_status_idle && dl->status != lora_datalink_status_rx_callback &&
       dl->status != lora_datalink_status_tx_callback ) {
    _datalink_error( "no estamos idle, no podemos recibir" );
    return -2;
  }
  dl->rx_cb = cb;
  dl->rx_cb_ctx = ctx;
  dl->seq_number = seq_number;
  dl->rx_src = src;

  NEXT_STATE( lora_datalink_status_rx_init );
  return 0;
}

int lora_datalink_run_one_iteration( struct lora_datalink_t *dl ) {
  _datalink_handle_new_state( dl );
  return 1;
}

int lora_datalink_init_done( struct lora_datalink_t *dl ) { return dl->init_done; }

/** Retorna si el datalink está en un estado de error irrecuperable. */
int lora_datalink_is_dead( struct lora_datalink_t *dl ) { return dl->status == lora_datalink_status_dead; }

int16_t lora_datalink_max_frame_length( struct lora_datalink_t *dl ) { return dl->link_conf._max_length; }
