/*=====[Module Name]===========================================================
 * Copyright YYYY Author Compelte Name <author@mail.com>
 * All rights reserved.
 * License: license text or at least name and link 
         (example: BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>)
 *
 * Version: 0.0.0
 * Creation Date: YYYY/MM/DD
 */
 
/*=====[Inclusion of own header]=============================================*/

#include "gpio_int.h"
#include "sapi.h"
#include "sapi_gpio.h"
#include "FreeRTOSConfig.h"

/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/

/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/
extern const pinInitGpioLpc4337_t gpioPinsInit[];
/*=====[Definitions of private global variables]=============================*/

/*=====[Prototypes (declarations) of private functions]======================*/
static void gpioObtainPinInit( gpioMap_t pin,
                               int8_t *pinNamePort, int8_t *pinNamePin,
                               int8_t *func, int8_t *gpioPort,
                               int8_t *gpioPin );

/*=====[Implementations of public functions]=================================*/

void gpioIntInit( gpioMap_t pin, gpioIntChannel_t channel, gpioIntInit_t intConfig){
	
	// Variables para guardar los datos de SCU y GPIO del puerto de la ciaa
	int8_t pinNamePort = 0;
	int8_t pinNamePin  = 0;
	int8_t func        = 0;
	int8_t gpioPort    = 0;
	int8_t gpioPin     = 0;

	gpioObtainPinInit( pin, &pinNamePort, &pinNamePin, &func, &gpioPort, &gpioPin);

	// Borro cualquier estado de condifugración previo
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(channel));

	Chip_PININT_Init(LPC_GPIO_PIN_INT);
	switch (intConfig)
	{
	case GPIO_INT_EDGE_RISING:
		// Mapeo del pin al canal (0 a 7) de la interrupción
		Chip_SCU_GPIOIntPinSel(channel,gpioPort,gpioPin);
		// Configuración de modo de interrupción
		Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT,PININTCH(channel));
		// Configuración de nivel de la interrupción
		Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT,PININTCH(channel));
		break;
	
	default:
		break;
	}

	// Limpio cualquier estado previo
	Chip_PININT_ClearRiseStates(LPC_GPIO_PIN_INT,PININTCH(0));
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(0));

	// Limpio flags de interrupción pendientes
	NVIC_ClearPendingIRQ(PIN_INT0_IRQn + channel);
	#ifdef USE_FREERTOS
	// Seteo la prioridad de la interrupciónm
	NVIC_SetPriority(PIN_INT0_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 1);
	#endif
	// Habilito interrupción del canal configurado
	NVIC_EnableIRQ(PIN_INT0_IRQn + channel);

//	gpioInit(pin,GPIO_INPUT);
}

/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/


static void gpioObtainPinInit( gpioMap_t pin,
                               int8_t *pinNamePort, int8_t *pinNamePin,
                               int8_t *func, int8_t *gpioPort,
                               int8_t *gpioPin )
{

   *pinNamePort = gpioPinsInit[pin].pinName.port;
   *pinNamePin  = gpioPinsInit[pin].pinName.pin;
   *func        = gpioPinsInit[pin].func;
   *gpioPort    = gpioPinsInit[pin].gpio.port;
   *gpioPin     = gpioPinsInit[pin].gpio.pin;
}
