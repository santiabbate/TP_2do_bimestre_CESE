/*=====[Module Name]===========================================================
 * Copyright YYYY Author Compelte Name <author@mail.com>
 * All rights reserved.
 * License: license text or at least name and link 
         (example: BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>)
 *
 * Version: 0.0.0
 * Creation Date: YYYY/MM/DD
 */

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _GPIO_INT_H_
#define _GPIO_INT_H_

/*=====[Inclusions of public function dependencies]==========================*/
#include "sapi.h"
/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/
typedef enum {
   GPIO_INT_EDGE_RISING
} gpioIntInit_t;

typedef enum {
   GPIO_INT_CH0,
   GPIO_INT_CH1,
   GPIO_INT_CH2,
   GPIO_INT_CH3,
   GPIO_INT_CH4,
   GPIO_INT_CH5,
   GPIO_INT_CH6,
   GPIO_INT_CH7
} gpioIntChannel_t;

/*=====[Prototypes (declarations) of public functions]=======================*/

void gpioIntInit(gpioMap_t pin, gpioIntChannel_t channel, gpioIntInit_t config);

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

void GPIO0_IRQHandler(void);

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _GPIO_INT_H_ */
