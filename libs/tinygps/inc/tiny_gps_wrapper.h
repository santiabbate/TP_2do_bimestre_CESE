/*=============================================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * Date: 2019/11/28
 * Version: 1
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef __TINY_GPS_H__
#define __TINY_GPS_H__

/*=====[Inclusions of public function dependencies]==========================*/

#include <stdint.h>
#include <stddef.h>
#include "sapi.h"

// #define millis() tickRead()

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/

void tiny_gps_init(uartMap_t uart_instance);

bool_t tiny_gps_data_available(uartMap_t uart_instance);

void tiny_gps_encode(uartMap_t uart_instance);

bool_t tiny_gps_time_isUpdated();

uint32_t tiny_gps_time_get_value();

uint32_t tiny_gps_time_get_hour();

uint32_t tiny_gps_time_get_minute();

uint32_t tiny_gps_time_get_second();

uint32_t tiny_gps_time_get_centisecond();

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* __TINY_GPS_H__ */
