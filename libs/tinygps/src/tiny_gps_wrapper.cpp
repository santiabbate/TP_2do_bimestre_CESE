/*=============================================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * Date: 2019/11/28
 * Version: 1
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "tiny_gps_wrapper.h"
#include "sapi.h"
#include "TinyGPS++.h"

/*=====[Definition macros of private constants]==============================*/

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

TinyGPSPlus gps;

void tiny_gps_init(uartMap_t uart_instance){
    uartInit(uart_instance,9600);
}

bool_t tiny_gps_data_available(uartMap_t uart_instance){
    return uartRxReady(uart_instance);
}

void tiny_gps_encode(uartMap_t uart_instance){
    uint8_t received_char = uartRxRead(uart_instance);
    gps.encode(received_char);
}

bool_t tiny_gps_time_isUpdated(){
    return gps.time.isUpdated();
}

uint32_t tiny_gps_time_get_value(){
    return gps.time.value();
}

uint32_t tiny_gps_time_get_hour(){
    return gps.time.hour();
}

uint32_t tiny_gps_time_get_minute(){
    return gps.time.minute();
}

uint32_t tiny_gps_time_get_second(){
    return gps.time.second();
}

uint32_t tiny_gps_time_get_centisecond(){
    return gps.time.centisecond();
}



