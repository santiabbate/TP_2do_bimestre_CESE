/**
 * @file timer_ciaa_rtos.c
 * @author Santiago Abbate
 * @brief Implementación de un timer para uso del protocolo loraSheep
 * @version 0.1
 * @date 2019-11-15
 * @copyright Copyright (c) 2019
 */

/*=====[Inclusion of own header]=============================================*/
#include "timer_CIAA_rtos.h"
#include "sapi.h"
/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/

/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Prototypes (declarations) of private functions]======================*/

/**
 * @brief Configuración del timer
 * Toma el período y el delay necesarios por el protocolo y establece
 * el "delta" en milisegundos para el timeout de la ciaa (target_ciaa_ticks)
 * @param timer_impl Puntero a la variable timer en uso ("Objeto" timer)
 */
static void setup_timer(timer_CIAA_impl_t * timer_impl);

/*=====[Implementations of public functions]=================================*/

void init_timer_impl(timer_CIAA_impl_t * timer_impl)
{  
   // Inicializo punteros a funciones   
   timer_iface_t *timer_iface = &(timer_impl->timer_iface);
   timer_iface->_private = timer_impl;
   timer_iface->_cancel_fn = &timer_impl_cancel;
   timer_iface->_synch_start_fn = &timer_impl_synch_start;
   timer_iface->_run_one_iteration_fn = &timer_impl_run_one_iteration;

   // Inicializo variables de control y callbacks
   timer_impl->target_ciaa_ticks = 0;
   timer_impl->gps_time_last_update = 0;
   timer_impl->period = 0;
   timer_impl->delay_ms = 0;
   timer_impl->started = 0;
   timer_impl->ticked = 0;
   timer_impl->timer_cb = 0;
   timer_impl->timer_cb_ctx = 0;
   timer_impl->timer_offset = 0;

}

/**
 * @brief Inicia el timer
 * Esta función es llamada por el protocolo loraSheep, y es mandatorio proveerla
 * @param handle Por este handle el protocolo loraSheep envía el "objeto" timer correspondiente
 * @param period 
 * @param delay_ms 
 * @param callback Recibe el callback a llamar cuando haya timeout
 * @param ctx Parámetros del callback
 * @return int 
 */
int timer_impl_synch_start(timer_handle_t handle,
                        int16_t period,
                        int16_t delay_ms,
                        timer_synch_start_callback_fn callback,
                        void *ctx )
{
    #ifdef TIMER_DEBUG
    printf("Synch start\r\n");
    #endif
    timer_CIAA_impl_t * timer = (timer_CIAA_impl_t *) handle;
    timer->period = period;
    timer->delay_ms = delay_ms;
    timer->timer_cb = callback;
    timer->timer_cb_ctx = ctx;
    timer->started = TRUE;
    // setup_timer configura los ticks para timeout
    setup_timer(handle);
}


int timer_impl_cancel( timer_handle_t handle )
{
	timer_CIAA_impl_t * timer = (timer_CIAA_impl_t *) handle;
    timer->started = FALSE;
    timer->ticked = FALSE;
    timer->timer_cb = 0;
    #ifdef TIMER_DEBUG
    printf("Timer cancelled \n");
    #endif
}

int timer_impl_run_one_iteration( timer_handle_t handle )
{
    timer_CIAA_impl_t * timer = (timer_CIAA_impl_t *) handle;
    bool_t ticked = FALSE;
    
	if (timer->started)
    {
        int64_t diff = (int64_t) tickRead() - (int64_t) timer->target_ciaa_ticks;
        if (diff >= 0)      //Obtengo si hubo "timeout" del timer mediante polling
        {
            ticked = TRUE;
        }
    }
    if (ticked && timer->timer_cb)
    {
        gpioToggle(LED1);
        #ifdef TIMER_DEBUG
        printf("--- Timeout\r\n");
        printf("--- %lu\r\n",tiny_gps_time_get_second());
        #endif
        // Ejecuto el callback, porque hubo "timeout" del timer
        timer->timer_cb(timer->timer_cb_ctx);
        timer->started = TRUE;
        setup_timer(timer);
    }    
}

/**
 * @brief Convierte la hora leída por el GPS a milisegundos desde las 00:00 (UTC)
 * 
 * @param timer_impl 
 */
void timer_update(timer_CIAA_impl_t * timer_impl){
    volatile tick_t time_ms = tiny_gps_time_get_hour();
    time_ms *= 60;
    time_ms += tiny_gps_time_get_minute();
    time_ms *= 60;
    time_ms += tiny_gps_time_get_second();
    time_ms *= 1000;

    #ifdef TIMER_DEBUG
			printf("--- Timer Update\r\n");
			printf("--- %lu\r\n",tiny_gps_time_get_second());
            #endif

    timer_impl->gps_time_last_update = time_ms;
}

/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/

static void setup_timer(timer_CIAA_impl_t * timer_impl)
{
    // Tomo el handle al timer
    timer_CIAA_impl_t * timer = timer_impl;

    if (!timer->started || timer->ticked) {
        return;     // Salgo si no está iniciado el timer, o todavía se está procesando el timeout (ticked)
      }

    // Este es el tiempo "Correcto" --> now_gps_time, tiene la última lectura del GPS
    static tick_t now_gps_time;
    now_gps_time = timer->gps_time_last_update;
    // Sumo correción a la lectura del GPS
    // (ticks desde la última sincronización con interrupción externa)
	now_gps_time += (tickRead() - timer->ticks_last_update);    

    // Calculo el tiempo objetivo en múltiplos del período deseado
    tick_t target_gps_ticks = ((now_gps_time + timer->period - 1) / timer->period);
    // Transformo el tiempo objetivo a ticks (milisegundos) y agrego el delay
    target_gps_ticks = target_gps_ticks * timer->period;
    target_gps_ticks += timer-> delay_ms;

    // Actualizo el tiempo objetivo de la plataforma
    int32_t milliseconds_delta = target_gps_ticks - now_gps_time;
    #ifdef TIMER_DEBUG
    printf("--- Delta: %d\r\n", milliseconds_delta);
    #endif
    timer->target_ciaa_ticks = milliseconds_delta + tickRead();
}

