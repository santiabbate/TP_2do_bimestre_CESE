/*=============================================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * Date: 2019/12/01
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/

#include "publishTask.h"
#include "loraTask.h"

#include "semphr.h"
 
/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/
#define PRINTF_THREAD_SAFE(string)  if (pdTRUE == xSemaphoreTake( mutex_uart_printf, portMAX_DELAY)){ \
   	                                 printf("%s\r\n",string);                                       \
                                       xSemaphoreGive( mutex_uart_printf );                           \
                                       }
/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/

extern timer_CIAA_impl_t timer_impl;

extern SemaphoreHandle_t mutex_uart_printf; //Mutex que protege la escritura por printf

extern xQueueHandle cola_coordenadas;

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Prototypes (declarations) of private functions]======================*/

/*=====[Implementations of public functions]=================================*/

// Task implementation
void taskPublish( void* taskParmPtr )
{
   publish_message_t message;
   // ----- Task setup -----------------------------------
   PRINTF_THREAD_SAFE( "Task Publish init" );

   /**
    * Acá inicializaría el bluetooth
    * Me quedé sin UARTs en la EDU-CIAA
    * UART0 --> Para leer la hora del GPS (UART_GPIO)
    * UART2 --> Para Debug (UART_USB)
    * UART3 --> Para RN2903 (UART_232)
    */

   // ----- Task repeat for ever -------------------------
   while(TRUE) {
      // Recibo bloqueando por siempre, un dato de la cola de mensajes, y luego lo imprimo

      xQueueReceive(cola_coordenadas, &message, portMAX_DELAY);
      PRINTF_THREAD_SAFE("----------------------");
      PRINTF_THREAD_SAFE("Coordendas recibidas: ");
      PRINTF_THREAD_SAFE(message.buffer);
      PRINTF_THREAD_SAFE("----------------------");
   }
}

/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/

