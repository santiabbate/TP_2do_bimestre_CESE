/*=============================================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * Date: 2019/12/01
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/

#include "gpsTask.h"
#include "../libs/tinygps/inc/tiny_gps_wrapper.h"
#include "timer_CIAA_rtos.h"

#include "semphr.h"
 
/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/
#define PRINTF_THREAD_SAFE(string)  if (pdTRUE == xSemaphoreTake( mutex_uart_printf, portMAX_DELAY)){ \
   	                                 printf("%s\r\n",string);                                       \
                                       xSemaphoreGive( mutex_uart_printf );                           \
                                       }
/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/

extern timer_CIAA_impl_t timer_impl;

extern SemaphoreHandle_t mutex_uart_printf; //Mutex que protege la escritura por printf

extern SemaphoreHandle_t smphr_first_tick;

extern SemaphoreHandle_t smphr_first_time_updated;

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Prototypes (declarations) of private functions]======================*/

/*=====[Implementations of public functions]=================================*/

// Task implementation
void taskParseGPS( void* taskParmPtr )
{
   // ----- Task setup -----------------------------------
   timer_CIAA_impl_t * timer = (timer_CIAA_impl_t *) taskParmPtr;      // Tomo el handle al timer de la librería loraSheep que paso por parámetro
   
   PRINTF_THREAD_SAFE( "Task Parse GPS init" );

   tiny_gps_init(UART_GPS);

   static bool_t first_read = FALSE;
      
   // Tomo semáforo para esperar el primer tick de TIMEPLUSE del GPS
   xSemaphoreTake(smphr_first_tick, portMAX_DELAY);

   // ----- Task repeat for ever -------------------------
   while(TRUE) {
      // El GPS escupe caracteres por la UART con las tramas NMEA
      // Mientras haya carateres disponibles, ejecuto la función de "encode" de la librería


      while(tiny_gps_data_available(UART_GPS))
      {
         tiny_gps_encode(UART_GPS);
      }

      if (tiny_gps_time_isUpdated())
      {
         if (!first_read)
         {
               // A este punto se hizo la primera lectura de la hora del GPS
               // Libero el semáforo para que la tarea LoRa pueda continuar
               xSemaphoreGive(smphr_first_time_updated);
               first_read = TRUE;
         }
         gpioToggle(LED3);
         timer_update(timer);
      }
   }
}

/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/

