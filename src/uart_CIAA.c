/*=====[Module Name]===========================================================
 * Copyright YYYY Author Compelte Name <author@mail.com>
 * All rights reserved.
 * License: license text or at least name and link 
         (example: BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>)
 *
 * Version: 0.0.0
 * Creation Date: YYYY/MM/DD
 */
 
/*=====[Inclusion of own header]=============================================*/

#include "uart_CIAA.h"
#include "ctype.h"
#include "sapi.h"
/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/


/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Prototypes (declarations) of private functions]======================*/

/*=====[Implementations of public functions]=================================*/

void init_uart_impl(uart_CIAA_impl_t * uart_impl)
{  
   // Inicializo punteros a funciones de la iterfaz uart (uart_iface_t)
   uart_iface_t *uart_iface = &(uart_impl->uart_iface);
   uart_iface->_private = uart_impl;
   uart_iface->_begin_readline = &begin_readline;
   uart_iface->_begin_write = &begin_write;
   uart_iface->_run_one_iteration = &run_one_iteration;
   uart_iface->_close = &close;

   // Inicializo variables de control y callbacks
   uart_impl->rx_fifo = 0;
   uart_impl->tx_fifo = 0;

   uart_impl->tx_cb = 0;
   uart_impl->tx_cb_ctx = 0;
   uart_impl->new_tx = 0;

   uart_impl->rx_cb = 0;
   uart_impl->rx_cb_ctx = 0;
   uart_impl->timeout_ms = 0;
   uart_impl->ticks_rx_start = 0;
   uart_impl->readlines = 0;

   uart_impl->got_cr = 0;
   uart_impl->reading = 0;

}

void start_uart_CIAA()
{
   //uartInit( UART_RN2903, 57600 );
   uartInit2( UART_RN2903, 57600, 8, FALSE, 1);
}

int begin_readline(uart_handle_t handle,
                        uart_fifo_t *buff,
                        uint8_t readlines,
                        uart_rx_callback_fn callback,
                        void *ctx,
                        int32_t timeout_ms )
{
   uart_CIAA_impl_t * uart = (uart_CIAA_impl_t *)handle; // Tomo el handle de la implementación

   // Vuelvo si no tengo buffer o callback disponibles
   if (!buff) {
   return 0;
   }
   if (!callback) {
      return 0;
   }
   
   uart->got_cr = false;
   uart->ticks_rx_start = tickRead();  // Ticks de inicio de rx
   uart->reading = true;
   uart->rx_cb = callback;
   uart->rx_cb_ctx = ctx;
   uart->rx_fifo = buff;
   uart->readlines = readlines;
   uart->timeout_ms = timeout_ms;

   #ifdef DEBUG_UART_RX_CHARS
   printf("Begin reading\r\n");
   #endif
   return 1; 
}

int begin_write(uart_handle_t handle,
                     uart_fifo_t *fifo,
                     uart_tx_callback_fn callback,
                     void *ctx )
{
   uart_CIAA_impl_t * uart = (uart_CIAA_impl_t *) handle;   // Tomo el handle de la implementación
   
   // Vuelvo si no tengo buffer o callback disponibles
   if (!fifo) {
      return 0;
   }
   if (!callback) {
      return 0;
   }
   uart->tx_cb = callback;
   uart->tx_cb_ctx = ctx;
   uart->tx_fifo = fifo;
   uart->new_tx = TRUE;
   return 1;
}

/**
 * @brief Actualiza el estado de la uartt
 * Procesa RX si hay disponible, o sale por timeout
 * Procesa TX si hay datos en la fifo de transmisión del protocolos
 * @param handle
 */
int run_one_iteration(uart_handle_t handle)
{
   uart_CIAA_impl_t * uart = (uart_CIAA_impl_t *) handle;

   enum uart_rx_status_e rx_status;
   bool_t available = uartRxReady(UART_RN2903); // Hay datos en la UART?
   bool_t call_cb = FALSE;
   
   enum uart_rx_status_e cb_status;

   bool_t atleastone = FALSE;

   // Proceso RX
   if(!uart->reading){  // Si no estoy leyendo, vacío lo que haya en la uart pq no lo estaba esperando
      if (available){
         #ifdef DEBUG_UART_RX_CHARS
         printf("RX ignored \r\n");
         #endif
         while (uartRxReady(UART_RN2903))
         {  
            uint8_t recv = uartRxRead(UART_RN2903);
            #ifdef DEBUG_UART_RX_CHARS
            printf("%c",isprint(recv) ? (char)recv : '.'); // Filtro los caracteres no imprimibles y cambio por '.'
            #endif
         }
         #ifdef DEBUG_UART_RX_CHARS
         printf("\r\n");
         #endif
      }   
   } else if(!available){ // Si estoy leyendo y no hay datos disponibles, espero timeout para llamar al callback
      if (uart->reading && (tickRead() - uart->ticks_rx_start > uart->timeout_ms)) {
         uart->reading = FALSE;
         // Hubo timeout --> Llamo al callback y le informo "uart_rx_timeout"
         uart->rx_cb(uart->rx_cb_ctx, uart_rx_timeout);
    }
   } else {					// Estoy leyendo y HAY datos
      while (uartRxReady(UART_RN2903) && uart_fifo_empty_space(uart->rx_fifo))
      {
         uint8_t data = uartRxRead(UART_RN2903);
         if (!atleastone)
         {
            atleastone = TRUE;
         #ifdef DEBUG_UART_RX_CHARS
            printf("RX ");
         #endif
         }
         #ifdef DEBUG_UART_RX_CHARS
         printf("%c",isprint(data) ? (char)data : '.');
         #endif
         
         uart_fifo_put(uart->rx_fifo, &data, 1);

         call_cb = TRUE;
         cb_status = uart_rx_ok;

         // Empiezo a procesar \r\n
         if(data == '\r'){
            uart->got_cr = TRUE;
         }
         else if (uart->got_cr && data == '\n'){ // Tengo \r y \n
            if (uart->readlines == 1){
               uart->reading = FALSE;
            }
            uart->readlines--;
            uart->got_cr = FALSE; 
         }

      }
      if (atleastone){
         #ifdef DEBUG_UART_RX_CHARS
         printf("\r\n");
         if (!uart->reading){
            printf("Done reading\r\n");
         }
         #endif
         uart->ticks_rx_start = tickRead();
      } else if (uart_fifo_empty_space(uart->rx_fifo) == 0){
         call_cb = TRUE;
         cb_status = uart_rx_buffer_overrun;
         uart->reading = FALSE;
      }
      
      if(call_cb) {
         uart->rx_cb(uart->rx_cb_ctx,cb_status);
      }
      
      
   } // End RX
   
   // Proceso TX

   volatile bool_t fifo_empty = uart_fifo_empty(uart->tx_fifo);
   bool_t printedtx = FALSE;

   while (!uart_fifo_empty(uart->tx_fifo)){
      
      if(!printedtx){
      #ifdef DEBUG_UART_TX_CHARS
         printf("TX ");
      #endif
         printedtx = TRUE;
      }
      
      uint8_t data;
      uart_fifo_get(uart->tx_fifo, &data);
      
      #ifdef DEBUG_UART_TX_CHARS
      printf("%c",data);
      #endif
      uartWriteByte(UART_RN2903,data);
   } 
   
   #ifdef DEBUG_UART_TX_CHARS
   if (printedtx){
      printf("\r\n");
   }
   #endif
   
   if (uart->new_tx || (printedtx && uart_fifo_empty(uart->tx_fifo))) {
    uart->new_tx = FALSE;
    uart->tx_cb(uart->tx_cb_ctx, uart_tx_fifo_empty);
  }
  // End TX

}

int close(uart_handle_t handle )
{

}

/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/


