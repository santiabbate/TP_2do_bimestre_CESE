/*=============================================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * Date: 2019/12/01
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/

#include "loraTask.h"
// #include "../libs/tinygps/inc/tiny_gps_wrapper.h"
// #include "timer_CIAA_rtos.h"
#include "../libs/gpio_int/inc/gpio_int.h"
#include <string.h>

#include "semphr.h"
 
/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/
#define GPS_TICK GPIO0
/*=====[Private function-like macros]========================================*/

#define PRINTF_THREAD_SAFE(string)  if (pdTRUE == xSemaphoreTake( mutex_uart_printf, portMAX_DELAY)){ \
   	                                 printf("%s\r\n",string);                                       \
                                       xSemaphoreGive( mutex_uart_printf );                           \
                                       }

/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/
extern xQueueHandle cola_coordenadas;
extern SemaphoreHandle_t mutex_uart_printf; //Mutex que protege la escritura por printf

extern SemaphoreHandle_t smphr_first_time_updated;
/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/
uint8_t receive_buffer[300];
   
bool_t can_receive = TRUE;

/*=====[Prototypes (declarations) of private functions]======================*/

void receive_callback(void *ctx, enum transport_rx_result_e * status) ;

/*=====[Implementations of public functions]=================================*/

int32_t init_loraSheep(struct transport_t * transport_layer, timer_CIAA_impl_t * timer_impl, uart_CIAA_impl_t * uart_impl)
{
   // Reseteo del módulo RN2903
   gpioInit(RN2903_REST_PIN,GPIO_OUTPUT);  
   gpioWrite(RN2903_REST_PIN,LOW);
   for(uint32_t i = 0; i < 10000; i++);
   gpioWrite(RN2903_REST_PIN,HIGH);

   // Creo timer para el protocolo loraSheep
   init_timer_impl(timer_impl);
   // Inicio la uart del protocolo
   init_uart_impl(uart_impl);
   start_uart_CIAA();

   // Inicio las capas del protocolo loraSheep
   if (transport_init(transport_layer, &(uart_impl->uart_iface), &(timer_impl->timer_iface), 0, 0, 0, 0))
   {
      printf("Transport init fail\r\n");
   }

   return 0;
}

// Task implementation
void taskLora( void* taskParmPtr )
{
   // ----- Task setup -----------------------------------
   loraTaskParam_t * taskParam = (loraTaskParam_t *) taskParmPtr;
   
   struct transport_t * transport_layer = taskParam->tp;  // Tomo el handle de la estructura de la capa de transporte
   timer_CIAA_impl_t * timer_impl = taskParam->timer_impl;
   uart_CIAA_impl_t * uart_impl = taskParam->uart_impl;

   PRINTF_THREAD_SAFE("Task Lora init");

   // La tarea se bloque esperando a que el se lea la hora sincronizada del GPS
   xSemaphoreTake(smphr_first_time_updated, portMAX_DELAY);
  
   // ----- Task repeat for ever -------------------------
   while(TRUE) {
      if (!transport_is_dead(transport_layer))
      {  
         // Ejecuto funciones propias de mi implementación
         timer_impl_run_one_iteration(timer_impl);
         uart_run_one_iteration(uart_impl);
         
         // Ejecuto funciones del protocolo loraSheep
         rn2903_run_one_iteration(&(transport_layer->dl.com));
         lora_datalink_run_one_iteration(&(transport_layer->dl));
         transport_run_one_iteration(transport_layer);

         // Si estoy disponible para recibir, Inicio nuevamente la recepción
         if(transport_init_done(transport_layer) && can_receive){

            PRINTF_THREAD_SAFE("Transport Layer Start receive");
            transport_receive(transport_layer, receive_buffer, 300, DATALINK_RECEIVE_ADDRESS, receive_callback, &transport_layer->rx_result);

            can_receive = FALSE;
         }

      }
      else
      {
         PRINTF_THREAD_SAFE("Transport_dead");
      }
      
         // vTaskDelay(1 / portTICK_RATE_MS);   
      
   }
}

/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/

// Callback que se ejecuta cuando termina la recepción de la capa de transporte
void receive_callback(void *ctx, enum transport_rx_result_e * status) {
   
   publish_message_t message;
   
   if (*status == transport_rx_err) {
	   PRINTF_THREAD_SAFE("Callback Receive: Transport error");
      return;
   }
   else{
      // Si la recepción terminó sin errores:
      // Armo el mensaje a enviar por la cola copiando del buffer de rececpción a mi estructura de mensaje
      memcpy(message.buffer, receive_buffer, BUFFER_LENGTH);
      // Envío lo que recibí a la cola de mensajes
      xQueueSend(cola_coordenadas, &message, portMAX_DELAY);
      // Estamos disponibles para recibir nuevamente
      can_receive = TRUE;
   }
   
}
