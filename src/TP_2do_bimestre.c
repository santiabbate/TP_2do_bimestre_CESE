/*=============================================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * Date: 2019/12/01
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "TP_2do_bimestre.h"

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

#include "sapi.h"
#include "gpsTask.h"
#include "loraTask.h"
#include "publishTask.h"

/*=====[Definition macros of private constants]==============================*/

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/
SemaphoreHandle_t mutex_uart_printf; //Mutex que protege la escritura por printf

SemaphoreHandle_t smphr_first_tick; // Semáforo para registrar el primer "TimePulse" del GPS

SemaphoreHandle_t smphr_first_time_updated; // Semáforo para registrar que se leyó por primera vez la hora actualizada del GPS

// Cola de mensajes entre tarea que recibe las coordenadas y tarea que las imprime por UART
xQueueHandle cola_coordenadas;

/*=====[Definitions of private global variables]=============================*/

// Estructura de la capa superior del protocolo lora
struct transport_t tp;

// Implementación del timer para el protocolo lora
timer_CIAA_impl_t timer_impl;

// Implementación de la uart para el protocolo lora
uart_CIAA_impl_t uart_impl;

loraTaskParam_t taskParam;

/*=====[Main function, program entry point after power on or reset]==========*/

int main( void )
 {
   int8_t error = 0;
   BaseType_t xReturned;
   boardInit();

   // Inicio protocolo loraSheep
   init_loraSheep(&tp, &timer_impl, &uart_impl);
   tp.dl.address = DATALINK_ADDRESS;
   
   taskParam.timer_impl = &timer_impl;
   taskParam.tp = &tp;
   taskParam.uart_impl = &uart_impl;
   

   // Creación de colas de mensajes
   if(NULL == (cola_coordenadas = xQueueCreate(5, sizeof(publish_message_t))))
   {
	   error = -1;
      printf("Error creando cola_coordenadas");
   }
   
   // Creación de Mutex
   if (NULL == (mutex_uart_printf = xSemaphoreCreateMutex())){
     	error = -1;
   }
   
   // Semáforo para registrar el primer "TimePulse" del GPS
   if (NULL == (smphr_first_tick = xSemaphoreCreateBinary())){
     	error = -1;
   }

   // Semáforo para registrar que se leyó por primera vez la hora actualizada del GPS
   if (NULL == (smphr_first_time_updated = xSemaphoreCreateBinary())){
     	error = -1;
   }
   
   // Creo tarea que parsea las tramas GPS leídas por la UART
   // Es una tarea continua
   xReturned = xTaskCreate(
				  taskParseGPS,
				  (const char *)"gpsTask",
				  configMINIMAL_STACK_SIZE*2,
				  &timer_impl,                  // Paso como parámetro el puntero al objeto timer del protocolo lorasSheep
				  tskIDLE_PRIORITY+1,
				  0
			   );

   if(xReturned == pdPASS){
	   printf("Create Task GPS OK\r\n");
   }
   else{
      error = -1;
   }

   // Creo tarea que mantiene el estado del protocolo loraSheep actualizado
   // Es una tarea continua
   xReturned = xTaskCreate(
				   taskLora,
				   (const char *)"loraTask",
				   configMINIMAL_STACK_SIZE*2,
				   &taskParam,                  //
				   tskIDLE_PRIORITY+1,
				   0
			      );

   if(xReturned == pdPASS){
      printf("Create Task Lora OK\r\n");
   }
   else{
      error = -1;
   }

   // Creo tarea que publica el resultado de las coordenadas GPS recibidas como un string a través de LoRa
   // Es una tarea que se bloquea esperando los datos por una cola
   xReturned = xTaskCreate(
				   taskPublish,
				   (const char *)"publishTask",
				   configMINIMAL_STACK_SIZE*2,
				   NULL,                  //
				   tskIDLE_PRIORITY+2,
				   0
			      );

   if(xReturned == pdPASS){
 	   printf("Create Task Publish OK\r\n");
   }
   else{
      error = -1;
   }

   // Inicio interrupciones externas para el tick del GPS
   gpioInit(GPS_TICK,GPIO_INPUT);
   gpioIntInit(GPS_TICK, GPIO_INT_CH0,GPIO_INT_EDGE_RISING);

   // Si todo estuvo bien, arranco el scheduler
   if(0 == error){
      vTaskStartScheduler(); // Initialize scheduler
   }
   else{
      printf("Error iniciando el sistema\r\n");
   }

   while( true ); // If reach heare it means that the scheduler could not start

   // YOU NEVER REACH HERE, because this program runs directly or on a
   // microcontroller and is not called by any Operating System, as in the 
   // case of a PC program.
   return 0;
}

void GPIO0_IRQHandler(){
   static bool_t first_tick = FALSE;
   
   BaseType_t xHigherPriorityTaskWoken = pdFALSE; //Comenzamos definiendo la variable
   gpioToggle(LED2);
   
   if (!first_tick)
   {
      // Libero semáforo
      xSemaphoreGiveFromISR(smphr_first_tick, &xHigherPriorityTaskWoken);
      first_tick = TRUE;
   }
   
   // Limpio flagas de la interrupción
   Chip_PININT_ClearRiseStates(LPC_GPIO_PIN_INT,PININTCH(0));
   Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT,PININTCH(0));

   timer_impl.ticks_last_update = xTaskGetTickCountFromISR();

   portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
