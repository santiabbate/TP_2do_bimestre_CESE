# 7.PCSE y 9.RTOS I

Trabajo final del segundo bimestre de la CESE
- Protocolos de comunicación en sistemas embebidosd
- Sistemas operativos de tiempo real I

## "Lectura de coordenadas GPS a través de módulo LoRa"

### Diagrama en bloques
![TP](Diagrama_TP.PNG)

### Diferencias con la propuesta original
En la propuesta original se iban a mostrar los datos recibidos por LoRa, en la pantalla de celular. Por tener sólo 3 UARTs disponibles y usar la interfaz de debug, la publicación de los datos a través de un puerto seria hacia el módulo bluetooth se reemplazó por una tarea que imprime por UART_DEBUG los datos recibidos.

### Resumen
El objetivo del programa es la implementación en la CIAA, de un protocolo desarrollado por Nicolás Bertolo en la UNRN, que puede realizar la comunicación entre un módulo LoRa funcionando como maestro y múltiples módulos funcionando como esclavos.

Para hacer una implementación de la librería del protocolo es necesario proveer dos interfaces:

1. Un timer (`timer_CIAA_rtos.c`) que implemente las siguientes funciones:
    * `timer_synch_start( timer_handle_t handle, int16_t period, int16_t delay_ms, timer_synch_start_callback_fn callback, void *ctx )`
        * Función synch_start. Inicia un timer con un `period` respecto al segundo 0.000 (UTC) del día.
      Agrega una desfasaje adicional a través de la opción `delay_ms`. Cuando hay `timeout`del 
      timer se ejecuta el `callback`
    * `timer_cancel( timer_handle_t handle )`
        * Cancela el timer. Si hay una ejecución pendiente también se cancela.
    * `timer_run_one_iteration( timer_handle_t handle )`
        * Actualiza el estado del timer. Si hubo `timeout` llama al callback.

2. Una UART (`uart_CIAA.c`) para el módulo bluetooth `uart_CIAA.c`
    * `uart_begin_readline( uart_handle_t handle, uart_fifo_t *buff, uint8_t readlines, uart_rx_callback_fn callback, void *ctx, int32_t timeout_ms )`
        * Inicia una lectura de línea (o líneas) a través de la interfaz UART. Cuando termina la lectura llama a `callback` con `context` como primer argumento. Si hubo timeout también llama al callback informando el timeout.
    * `uart_begin_write( uart_handle_t handle, uart_fifo_t *fifo, uart_tx_callback_fn callback, void *ctx )`
        * Inicia una escritura. Cuando se vacie la FIFO se llama a `callback` con `context` como primer argumento.
    *   `uart_close( uart_handle_t handle )`
        * Termina la comunicación
    * `uart_run_one_iteration( uart_handle_t handle )`
        * Actualiza el estado de la UART. Llama a los callbacks si es necesario

### Interfaces
* UART_DEBUG de la EDU_CIAA --> Impresión de mensajes 
* UART_232 de la EDU_CIAA --> Conexión a módulo RN2903
* UART_GPIO de la EDU_CIAA --> Conexión a módulo GPS

### Librerías
En `\libs` incluyo librerías propias del proyecto (Ver inclusión en `config.mk`)
* gpio_int : Librería para inicialización de interrupciones externas por GPIO (La librería no está completa, por ahora sólo agregué lo que necesitaba para el proyecto).
* lorasheep : Funciones del protocolo
* tinygps : Wrapper en C para funcines de librería TinyGPS++ de Arduino (El wrapper no está completo, por ahora sólo implementé las funciones que necesitaba para la lectura de la hora del GPS). 

### RTOS

#### Tareas
En total hay 3 tareas, más un servicio de interrupciones por flanco ascendente de GPIO externo.
El siguiente diagrama muestra un ejemplo de ejecución una vez que el sistema ya está inicializado.

![TP](Diagrama_de_tareas.png)


La próxima imagen muestra, además, el mecanismo de sincronización inicial, a través del cual la tarea gpsTask se inicializa y luego se bloquea hasta recibir el primer "Timepulse" del GPS a través de un semáforo. 

La tarea loraTask también se inicializa y luego se bloquea hasta que la tarea gpsTask haya procesado la primer trama NMEA y obtenido la hora UTC actualizada. Luego de eso libera el semáforo y la tarea loraTask puede continuar.

![TP](Diagrama_Tareas_inicio.png)

#### Descripción y Prioridades

* **loraTask**: Esta tarea está atada a la implementación del protocolo y es una tarea continua que mantiene el estado de las capas y de las implementaciones propias de la plataforma (El timer y la UART)
    * _Prioridad:_ tskIDLE_PRIORITY+1

* **gpsTask**: Esta tarea utiliza un wrapper de la librería en C++, [TinyGPSPlus](http://arduiniana.org/libraries/tinygpsplus/). También es una tarea continua, que va recibiendo caracteres de la trama NMEA del GPS y decodificandola.
    * _Prioridad:_ tskIDLE_PRIORITY+1
    Estas dos primeras tareas tienen la misma prioridad ya que son tareas "continuas" y el scheduler va aleternando su ejecución. En `freeRTOSConfig.h` verifiqué la definición de `configUSE_PREEMPTION` en `1`.

* **publishTask**: Esta tarea queda continuamente bloqueada esperando la recepción de datos a través de una cola. Recibe un string con los datos recibidos en la tarea `loraTask` y los imprime por `UART_DEBUG`. Originalmente esta tarea es la encargada de enviar la misma trama de datos( que ahora se imprime), al módulo bluetooth, para ver las coordenadas en el celular.
    * _Prioridad:_ tskIDLE_PRIORITY+2
    Es la tarea de prioridad más alta, para que al desbloquearse cuando recibe un dato por la cola de recepción, ejecute la muestra de los datos, y vualva a bloquearse a la espera de más datos.


### Modularización

* Programa principal
    * `TP_2do_bimestre.c`
* Tareas
    * `gpsTask.c`
    * `loraTask.c`
    * `publishTask.c` 
* Timer y UART para el protocolo loraSheep
    * `timer_CIAA_rtos.c`
    * `uart_CIAA.c`

### Video de ejemplo de uso
(https://gitlab.com/santiabbate/TP_2do_bimestre_CESE/blob/master/TP_2do_Bimestre.mp4?expanded=true&viewer=rich)