import os
import sys
import threading
import time

import pylorasheep
import pylorasheep.clib as clib
import pylorasheep.event_loop
import pylorasheep.timer
import pylorasheep.uart_pyserial


class _initCallback(clib.transport_init_cb_wrapper):
    def __init__(self):
        self.init_done = False
        super().__init__()

    def __call__(self, status):
        self.init_done = True
        self.init_status = status
        print("Python init callback, init status = " + str(status))


class _TxCallback(clib.transport_tx_cb_wrapper):
    def __init__(self):
        self.done = False
        super().__init__()

    def __call__(self, status):
        print("Python tx callback, tx status = " + str(status))
        if (status == 0):
            print("\n\n\t\tDatos Enviados, ack recibido\n\n")
        self.done = True


transport = clib.transport_t()
datalink = transport.dl
rn2903_com = datalink.com

event_loop = pylorasheep.event_loop.EventLoop()
uart = pylorasheep.uart_pyserial.UART("spy:///dev/ttyACM0?file=test_tx.txt", event_loop)
uart_handle = uart.get_uart_iface()
timer = pylorasheep.timer.Timer(event_loop)
timer_handle = timer.get_timer_iface()


def loop():
    while True:
        event_loop.wait_until_event()
        clib.uart_run_one_iteration(uart_handle)
        timer.run_one_iteration()
        clib.rn2903_run_one_iteration(rn2903_com)
        clib.lora_datalink_run_one_iteration(datalink)
        clib.transport_run_one_iteration(transport)


loop_thread = threading.Thread(target=loop, daemon=True)
loop_thread.start()

init_callback = _initCallback()

clib.transport_init(
    transport,
    uart_handle,
    timer_handle,
    clib.call_event_loop_wrapper,
    event_loop,
    clib.call_transport_init_cb_wrapper,
    init_callback,
)

datalink.address = 0x1
dst = 0x2
data = b"LAT,-41.1248548,LON,-71.2509114"
print(data.hex())
tx_callback = _TxCallback()

# print(data)
# print(len(data))

while not init_callback.init_done:
    time.sleep(0.1)

if init_callback.init_status != clib.lora_datalink_init_ok:
    sys.exit(1)

while True:
    tx_callback.done = False
    clib.transport_send(transport, dst, data, clib.call_transport_tx_cb_wrapper, tx_callback)
    print("Send")
    # time.sleep(10);
    while not tx_callback.done:
        time.sleep(0.1)
