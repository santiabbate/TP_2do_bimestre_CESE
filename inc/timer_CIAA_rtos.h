/**
 * @file timer_ciaa_rtos.h
 * @author Santiago Abbate
 * @brief Implementación de un timer para uso del protocolo loraSheep
 * @version 0.1
 * @date 2019-11-15
 * @copyright Copyright (c) 2019
 */

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _TIMER_CIAA_H_
#define _TIMER_CIAA_H_

/*=====[Inclusions of public function dependencies]==========================*/
#include "sapi.h"

// Libs includes
#include "../libs/lorasheep/transport.h"
#include "../libs/lorasheep/uart.h"
#include "../libs/tinygps/inc/tiny_gps_wrapper.h"

// RTOS includes
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/
#define UART_GPS UART_GPIO      // Entrada de interrupción del tick del GPS

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

/**
 * @struct Estructura de control del timer
 * @var timer_iface Estructura del protocolo loraSheep con los punteros a las funciones "mandatorias"
 * @var target_ciaa_ticks Tiempo objetivo para el timeout en la ciaa
 * @var gps_time_last_update Última actualización de la hora del GPS en milisegundos desde el cero (UTC) 
 * @var ticks_last_update Ticks del sistema en la última actualización de la hora del GPS   
 * @var period Tiempo del período correspondiente
 * @var delay_ms Delay extra por sobre el período
 * @var timer_cb Callback para ejecutar luego de un timeout
 * @var timer_cb_ctx Parámetros del callback
 */
typedef struct
{
    timer_iface_t timer_iface;
    tick_t target_ciaa_ticks;    
    tick_t gps_time_last_update;
	tick_t ticks_last_update;
    tick_t period;
    tick_t delay_ms;
    volatile bool_t started;
    volatile bool_t ticked;
    timer_synch_start_callback_fn timer_cb;
    void *timer_cb_ctx;
    long timer_offset;
}timer_CIAA_impl_t;


/*=====[Prototypes (declarations) of public functions]=======================*/
/**
 * @brief Inicialización del timer.
 * Carga la estructura con los punteros a las funciones que requiere loraSheep
 * Inicializa los campos
 * @param timer_impl Puntero a la variable timer en uso ("Objeto" timer)
 */
void init_timer_impl(timer_CIAA_impl_t * timer_impl);

int timer_impl_synch_start(timer_handle_t handle,
                        int16_t period,
                        int16_t delay_ms,
                        timer_synch_start_callback_fn callback,
                        void *ctx );

/**
 * @brief Cancela el timer
 * Esta función es llamada por el protocolo loraSheep, y es mandatorio proveerla
 * @param handle 
 * @return int 
 */
int timer_impl_cancel( timer_handle_t handle );

/**
 * @brief Actualiza el estado del timer
 * Implementación de timer por software. Compara un valor objetivo de ticks con los ticks actual del sistema
 * Llama a callback cuando haya timeout
 * @param handle  
 * @return int 
 */
int timer_impl_run_one_iteration( timer_handle_t handle );

void timer_update();

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _TIMER_CIAA_H_ */
