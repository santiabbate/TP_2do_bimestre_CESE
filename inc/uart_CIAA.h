/**
 * @file timer_ciaa_rtos.h
 * @author Santiago Abbate
 * @brief Implementación de una interfaz de UART para uso del protocolo loraSheep
 * @version 0.1
 * @date 2019-11-15
 * @copyright Copyright (c) 2019
 */

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _UART_CIAA_H_
#define _UART_CIAA_H_

/*=====[Inclusions of public function dependencies]==========================*/
#include "sapi.h"
#include "../libs/lorasheep/transport.h"
#include "../libs/lorasheep/uart.h"
/*=====[C++ - begin]=========================================================*/
#ifdef __cplusplus
extern "C" {
#endif
/*=====[Definition macros of public constants]===============================*/
#define UART_RN2903 UART_232
/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/
/**
 * @struct Estructura de control de la UART
 */
typedef struct
{
   // Estructura del protocolo loraSheep con los punteros a las funciones "mandatorias"
   uart_iface_t uart_iface;
   // Tx y Rx Fifos
   uart_fifo_t *rx_fifo;
   uart_fifo_t *tx_fifo;

   uart_tx_callback_fn tx_cb ;
   void *tx_cb_ctx;
   bool new_tx;

   uart_rx_callback_fn rx_cb;
   void *rx_cb_ctx;
   int32_t timeout_ms;
   tick_t ticks_rx_start;
   uint8_t readlines;

   int got_cr;
   int reading;
}uart_CIAA_impl_t;


/*=====[Prototypes (declarations) of public functions]=======================*/
/**
 * @brief Inicialización de la UART
 * Carga la estructura con los punteros a las funciones que requiere loraSheep
 * Inicializa los campos
 * @param uart_impl Puntero a la variable Uart en uso ("Objeto" UART)
 */
void init_uart_impl(uart_CIAA_impl_t * uart_impl);

/**
 * @brief Inicia la UART para conexión del módulo RN2903
 */
void start_uart_CIAA();


int begin_readline(uart_handle_t handle,
                        uart_fifo_t *buff,
                        uint8_t readlines,
                        uart_rx_callback_fn callback,
                        void *ctx,
                        int32_t timeout_ms );


int begin_write(uart_handle_t handle,
                     uart_fifo_t *fifo,
                     uart_tx_callback_fn callback,
                     void *ctx );


/**
 * @brief Actualiza el estado de la uartt
 * Procesa RX si hay disponible, o sale por timeout
 * Procesa TX si hay datos en la fifo de transmisión del protocolos
 * @param handle
 */
int run_one_iteration(uart_handle_t handle);

int close(uart_handle_t handle );

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _UART_CIAA_H_ */
