/*=============================================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * Date: 2019/12/01
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef __TP_2DO_BIMESTRE_H__
#define __TP_2DO_BIMESTRE_H__

/*=====[Inclusions of public function dependencies]==========================*/

#include <stdint.h>
#include <stddef.h>

// Incluyo libs
#include "../libs/lorasheep/transport.h"
#include "uart_CIAA.h"
#include "../libs/gpio_int/inc/gpio_int.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/
#define GPS_TICK GPIO0

#define OVERRIDE_SAPI_HCSR04_GPIO_IRQ

#define DATALINK_ADDRESS 2

// La tarea que parsea el GPS y la que actualiza el estado del protocolo lorasheep
// son tareas continuas que van a estar ejecutando alternadamente y por eso tienen
// la misma prioridad
#define PRIORIDAD_TASK_GPS 1
#define PRIORIDAD_TASK_LORA 1
// La tarea que publica los datos recibidos a través de LoRa se bloquea hasta 
// recibir datos por una cola y una vez que recibe tiene que imprimirlos.
// Es de mayor prioridad que las otras dos tareas
#define PRIORIDAD_TASK_PUBLISH 2

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/

/*=====[Prototypes (declarations) of public interrupt functions]=============*/
void GPIO0_IRQHandler();
/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* __TP_2DO_BIMESTRE_H__ */
