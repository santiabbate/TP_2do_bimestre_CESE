/*=============================================================================
 * Author: Santiago Abbate <sabbate@unrn.edu.ar>
 * Date: 2019/12/01
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef __LORA_TASK_H__
#define __LORA_TASK_H__

/*=====[Inclusions of public function dependencies]==========================*/

#include "FreeRTOS.h"
#include "task.h"
#include "sapi.h"

#include "timer_CIAA_rtos.h"
#include "../libs/lorasheep/transport.h"
#include "uart_CIAA.h"
/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/
#define UART_RN2903 UART_232
#define RN2903_REST_PIN GPIO8

#define DATALINK_RECEIVE_ADDRESS 1

#define BUFFER_LENGTH 300
/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/
// Estructura de parámetros para la creación de la tarea
// Contiene punteros a la capa de transporte (capa superior) y las implementaciones del timer y de la uart
typedef struct
{
    struct transport_t * tp;
    timer_CIAA_impl_t * timer_impl;
    uart_CIAA_impl_t * uart_impl;
}loraTaskParam_t;

// Estructura de los mensajes que envío y recibo a través de la cola de mensajes
typedef struct
{
    uint8_t buffer[BUFFER_LENGTH];
}publish_message_t;


/*=====[Prototypes (declarations) of public functions]=======================*/

int32_t init_loraSheep(struct transport_t * transport_layer, timer_CIAA_impl_t * timer_impl, uart_CIAA_impl_t * uart_impl);

void taskLora( void* taskParmPtr );  // Task declaration

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* __LORA_TASK_H__ */
